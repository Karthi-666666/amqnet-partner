import React from 'react';
import { NavigationActions } from "react-navigation";
import { DrawerActions } from 'react-navigation-drawer';
import { Image, Text, View, TouchableOpacity, ScrollView, AsyncStorage } from "react-native";
import { Container, Content } from "native-base";
let Params = require('../assets/json/Params')
import Api from '../http_services/Api';
import Color from '../assets/colors/colors';
import { ToastMsg } from '../assets/helper/Helper';
import Loading from '../assets/loading/Loading';

export default class DrawerScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nameUser: "",
            convertedImages: '',
            visibleModal: 0,
            invitesCountMe: '',
            visibleModalPrivacy: null,
            user_details: '',
            isLoading: false
        };
    }

    async  UNSAFE_componentWillMount() {
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
    }

    async  UNSAFE_componentWillUpdate() {
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
    }

    componentDidMount() {
        AsyncStorage.getItem('VENDOR_DETAILS').then((token) => {
            this.setState({
                isLoading: false
            });
        });
    }

    navigateToScreen = route => () => {
        const navigateAction = NavigationActions.navigate({
            routeName: route
        });
        this.props.navigation.dispatch(navigateAction);
        this.props.navigation.dispatch(DrawerActions.closeDrawer());
    };

    Home = () => {
        this.props.navigation.navigate('Dashboard')
        this.props.navigation.closeDrawer()
    }

    User_Profile = () => {
        this.props.navigation.navigate('User_Profile')
        this.props.navigation.closeDrawer()
    }

    Change_Password = () => {
        this.props.navigation.navigate('Change_Password')
        this.props.navigation.closeDrawer()
    }

    Bank_Details = () => {
        this.props.navigation.navigate('Bank_Details')
        this.props.navigation.closeDrawer()
    }

    async logout() {
        this.setState({ isLoading: true })
        let TOKEN = await AsyncStorage.getItem('Vendor_fcmToken');
        Params.Sign_Out.user_ids = this.state.user_details[0].user_id,
            Params.Sign_Out.device_tokens = TOKEN,
            Api.Post_SignOut(Params.Sign_Out, this.HandleLogout_Response, this.HandleLogout_Error)
    }

    HandleLogout_Response = async (response) => {
        if (response.status == 'success') {
            AsyncStorage.removeItem('VENDOR_STAY_LOGGED_IN');
            AsyncStorage.removeItem('VENDOR_DETAILS');
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ isLoading: false })
            this.props.navigation.navigate('Login')
        }
        else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ isLoading: false })
        }
    }

    HandleLogout_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ isLoading: false })
    }

    render() {
        return (
            <Container>
                {this.state.user_details ?
                    <View style={{ borderBottomColor: Color.common_Black, borderBottomWidth: 1, height: '20%', alignItems: 'center', justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => { this.User_Profile() }} >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <View>
                                    {this.state.user_details[0].user_image_id == null ?
                                        <Image
                                            source={require('../assets/images/default_profile.png')}
                                            style={{ borderRadius: 60, height: 60, width: 60 }} />
                                        : <Image
                                            source={{ uri: this.state.user_details[0].user_image_id }}
                                            style={{ borderRadius: 60, height: 60, width: 60 }} />
                                    }
                                    <View style={{ backgroundColor: Color.common_Green, position: 'absolute', top: '60%', left: '70%', width: 30, height: 20, borderRadius: 2, justifyContent: 'center' }}>
                                        <Text style={{ fontSize: Color.common_XSmall, fontFamily: 'Segoe UI', color: Color.common_White, alignSelf: 'center' }}>Edit</Text>
                                    </View>
                                </View>
                                <View style={{ marginLeft: '10%' }}>
                                    <Text numberOfLines={1} style={{ fontSize: Color.common_Medium, width: 150, fontFamily: 'Segoe UI' }} >{this.state.user_details[0].user_name}</Text>
                                    <Text style={{ fontSize: Color.common_Small, marginTop: '3%', fontFamily: 'Segoe UI' }} >{this.state.user_details[0].mobile}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View> : null
                }
                <Content>
                    <ScrollView>
                        <View>
                            <View style={{ marginTop: '2%' }}>
                                <TouchableOpacity onPress={() => { this.Home() }} >
                                    <View style={{ flexDirection: 'row', marginVertical: '3%' }}>
                                        <View style={{ marginHorizontal: '10%' }}>
                                            <Image
                                                source={require('../assets/images/Home.png')}
                                                style={{ height: 20, width: 20 }} />
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: Color.common_Medium, fontFamily: 'Segoe UI' }}>Home</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => { this.User_Profile() }}>
                                    <View style={{ flexDirection: 'row', marginVertical: '3%' }}>
                                        <View style={{ marginHorizontal: '10%' }}>
                                            <Image
                                                source={require('../assets/images/Account.png')}
                                                style={{ height: 20, width: 20 }} />
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: Color.common_Medium, fontFamily: 'Segoe UI' }}>My Profile</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => { this.Bank_Details() }}>
                                    <View style={{ flexDirection: 'row', marginVertical: '3%' }}>
                                        <View style={{ marginHorizontal: '10%' }}>
                                            <Image
                                                source={require('../assets/images/bank.png')}
                                                style={{ height: 20, width: 20 }} />
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: Color.common_Medium, fontFamily: 'Segoe UI' }}>Bank Details</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <TouchableOpacity onPress={() => { this.Change_Password() }}>
                                    <View style={{ flexDirection: 'row', marginVertical: '3%' }}>
                                        <View style={{ marginHorizontal: '10%' }}>
                                            <Image
                                                source={require('../assets/images/password.png')}
                                                style={{ height: 20, width: 20 }} />
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: Color.common_Medium, fontFamily: 'Segoe UI' }}>Change Password</Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
                <View style={{ borderTopColor: '#000', borderTopWidth: 1, height: '10%', justifyContent: 'center' }}>
                    {this.state.isLoading ? <Loading /> :
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ marginHorizontal: '10%' }}>
                                <TouchableOpacity onPress={() => { this.logout() }}>
                                    <Image
                                        source={require('../assets/images/logout.png')}
                                        style={{ height: 30, width: 30 }} />
                                </TouchableOpacity>
                            </View>
                            <View style={{ alignSelf: 'center' }}>
                                <TouchableOpacity onPress={() => { this.logout() }}>
                                    <Text style={{ fontSize: Color.common_Large, fontFamily: 'Segoe UI Bold' }}>Logout</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    }
                </View>
            </Container>
        )
    }
}

