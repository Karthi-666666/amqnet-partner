
import React from 'react';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Splash from '../screens/splash/Splash';
import Login from '../screens/login/Login';
import Register from '../screens/register/Register';
import Terms_Conditions from '../screens/register/Terms&Conditions';
import Privacy_Policy from '../screens/register/privacy_policy';
import Dashboard from '../screens/Dashboard/Dashboard';
import User_Profile from '../screens/user_profile/User_Profile';
import Edit_Documents from '../screens/user_profile/Edit_Documents';
import Add_Documents from '../screens/user_profile/Add_Documents';
import Change_Password from '../screens/change_password/Change_Password';
import Forgot_Password from '../screens/forgot_password/Forgot_Password';
import Reset_Password from '../screens/Reset_password/Reset_Password';
import OTP from '../screens/otp_verification/Otp_Verification';
import Complaint_Details from '../screens/complaints/Complaint_Details';
import Notifications from '../screens/notification/Notifications';
import Upload_Documents from '../screens/register/Upload_Documents';
import Bank_Details from '../screens/bank_details/Bank_Details';
import DrawerScreen from './DrawerScreen';

const AppNavigator = createStackNavigator({
    Splash: { screen: Splash },
    Login: { screen: Login },
    Register: { screen: Register },
    Terms_Conditions: { screen: Terms_Conditions },
    Privacy_Policy: { screen: Privacy_Policy },
    Dashboard: { screen: Dashboard },
    Change_Password: { screen: Change_Password },
    Forgot_Password: { screen: Forgot_Password },
    Reset_Password: { screen: Reset_Password },
    OTP: { screen: OTP },
    User_Profile: { screen: User_Profile },
    Edit_Documents: { screen: Edit_Documents },
    Add_Documents: { screen: Add_Documents },
    Complaint_Details: { screen: Complaint_Details },
    Upload_Documents: { screen: Upload_Documents },
    Notifications: { screen: Notifications },
    Bank_Details: { screen: Bank_Details },
},
    {
        initialRouteName: "Splash",
    }
);

AppNavigator.navigationOptions = ({ navigation }) => {
    name = (navigation.state.index !== undefined ? navigation.state.routes[navigation.state.index] : navigation.state.routeName)
    let drawerLockMode = 'locked-closed'
    if (name.routeName == 'Dashboard' || name.routeName == 'User_Profile' || name.routeName == 'Complaint_Details' || name.routeName == 'Notifications' || name.routeName == 'Change_Password' || name.routeName == 'Bank_Details') {
        drawerLockMode = 'unlocked'
    }
    return {
        drawerLockMode,
    };
};

const stackNavigation = createDrawerNavigator(
    {
        DrawerScreen: { screen: DrawerScreen },
        AppNavigator: AppNavigator,
    },
    {
        initialRouteName: "AppNavigator",
        hideStatusBar: false,
        drawerPosition: "left",
        activeTintColor: '#fff',
        activeBackgroundColor: '#6b52ae',
        contentComponent: ({ navigation }) => (
            <DrawerScreen navigation={navigation} />
        )
    }
);

const MyDrawerNavigator = createAppContainer(
    createSwitchNavigator({
        Main: stackNavigation,
    })
);

export default MyDrawerNavigator;

