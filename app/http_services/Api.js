import axios from 'axios';
import { Api_Config } from './Api_Config';

export default class Api {

    static get_Apartment_list(response, error) {
        let params = {};
        let Api_Name = Api_Config.Apartment_List;
        sendHttpRequest(Api_Name, params, response, error, true);
    }
    static get_Vendors_Type_list(response, error) {
        let params = {};
        let Api_Name = Api_Config.Vendors_Type_list;
        sendHttpRequest(Api_Name, params, response, error, true);
    }
    static Get_Payout_Contact_ID(response, error) {
        let params = {};
        let Api_Name = Api_Config.VendorPayoutContactID;
        sendHttpRequest(Api_Name, params, response, error, true);
    }
    static get_Vendor_task_cancel_reasons(response, error) {
        let params = {};
        let Api_Name = Api_Config.Vendor_task_cancel_reasons;
        sendHttpRequest(Api_Name, params, response, error, true);
    }
    static Post_Login(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Login;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_SignUp(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.SignUp;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Terms_and_Condition(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Terms_and_Condition;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Privacy_Policy(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Privacy_Policy;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Signupimageupload(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Signupimageupload;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Signupimageinsert(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Signupimageinsert;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_RegisterVerify_OTP(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Register_Verify_OTP;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Resend_otp(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Resend_OTP;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_ForgotPassword(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Forgot_Password;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_ForgotVerify_Otp(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Forgot_Otp;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_ResetPassword(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Reset_PassWord;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_UsersDetails(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.UsersDetails;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_EditProfile(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.EditProfile;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_UserImageUpload(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.UserImageUpload;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_ProfileimageUpdate(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.ProfileimageUpdate;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Edit_Doc_Upload(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Edit_Doc_Upload;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Edit_Doc_Update(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Edit_Doc_Update;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Add_Get_Doc(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Add_Get_Doc;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Add_Doc_Upload(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Add_Doc_Upload;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Add_Doc_Update(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Add_Doc_Update;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Changepassword(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Changepassword;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Insert_Token(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Insert_Token;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_SignOut(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.SignOut;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_task_pending_list(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_task_pending_list;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_NotificationList(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_NotificationList;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_task_accepted_list(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_task_accepted_list;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_TaskAccept(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Task_Accept;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_extra_amount_request(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_extra_amount_request;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Task_Cancel(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Task_Cancel;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Bank_Details(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Bank_Details;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_VendorsVerify_OTP(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.VendorsVerify_OTP;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_start_work(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_start_work;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_end_work(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_end_work;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Quit_Task(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Quit_Task;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
    static Post_Vendor_Accepted_Complaints_Viewdetails(param, response, error) {
        let params = param;
        let Api_Name = Api_Config.Vendor_Accepted_Complaints_Viewdetails;
        sendHttpRequest(Api_Name, params, response, error, false);
    }
}

async function sendHttpRequest(Api_Name, params, responseCallBack, errorCallBack, isGetRequest) {
    var headers = {
        'headers': {
            'Content-Type': 'application/json',
        }
    }

    var requestType = 'POST';
    if (isGetRequest) {
        requestType = 'GET';
    }
    var base_url = Api_Config.Base_URL;
    try {
        if (isGetRequest) {
            axios.get(base_url + Api_Name, headers)
                .then(res => {
                    return responseCallBack(res.data);
                })
                .catch(function (thrown) {
                    return errorCallBack(thrown.message);
                });
        } else {
            axios.post(base_url + Api_Name, params, headers)
                .then(res => {
                    return responseCallBack(res.data);
                })
                .catch(function (thrown) {
                    return errorCallBack(thrown.message);
                });
        }
    } catch (error) {
        return errorCallBack(error);
    }
}
