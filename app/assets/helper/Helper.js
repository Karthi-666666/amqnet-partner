import { Toast } from 'native-base';

export const ToastMsg = async (type, msg, color) => {
    Toast.show({
        marginTop: "20",
        position: "bottom",
        text: msg,
        type: type,
        textStyle: { color: "#FFF", fontSize: 16, textAlign: 'center', fontFamily: 'Segoe UI' },
        duration: 5000,
        style: {
            backgroundColor: color
        }
    });
}