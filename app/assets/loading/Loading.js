import React from "react";
import { Spinner, View } from "native-base";

export default class Loader extends React.Component {
  
  render() {
    return (
      <View style={{ flex: 1, flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
        <Spinner color="#E67E22" />
      </View>
    );
  }
}
