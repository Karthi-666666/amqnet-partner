const colors = {
    Orange_color: '#E67E22',
    red_color: '#FF0000',
    gray_color: '#c8c8c7',

    // Colors
    common_White: '#fff',
    common_Black: '#000',
    common_gray: '#C8C8C7',
    common_Green: '#5E6E3B',
    common_Red: '#f50707',

    // Text Color
    Txt_Gray: '#69695f',
    Txt_Blue: '#074af0',
    Btn_Open: '#f11d1d',
    Btn_Pending: '#d6ce5a',
    Btn_Closed: '#267e15',
    Btn_Canceled: '#c8c8c7',

    // Font Size
    common_XSmall: 12,
    common_Small: 14,
    common_Medium: 16,
    common_Large: 20,
    common_MLarge: 22,
    common_XLarge: 24,
    font_13: 13,
    
    // Toast Color
    toast_danger: '#D13427',
    toast_success: '#818d66',
}

export default colors;

