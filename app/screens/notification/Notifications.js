import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Text, Title, Body, Card, CardItem, } from 'native-base';
import ViewMoreText from 'react-native-view-more-text';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Loading from '../../assets/loading/Loading';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import moment from 'moment';

export default class Notification extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            Notification_Data: [],
            is_loading: true,
            no_data: '',
        };
    }

    async  componentWillMount() {
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        Params.Notifications.user_id = this.state.user_details[0].user_id,
            Api.Post_NotificationList(Params.Notifications, this.HandleNotifications_Response, this.HandleNotifications_Error)
    }

    HandleNotifications_Response = (response) => {
        if (response.status == 'success') {
            this.setState({
                Notification_Data: response.response,
                is_loading: false
            })
        } else {
            this.setState({ is_loading: false, no_data: response.message, })
        }
    }

    HandleNotifications_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    renderViewMore(onPress) {
        return (
            <Text onPress={onPress} style={Common_Style.Notification_More_Txt}>View More</Text>
        )
    }

    renderViewLess(onPress) {
        return (
            <Text onPress={onPress} style={Common_Style.Notification_More_Txt}>View less</Text>
        )
    }

    render() {
        return (
            <Container style={Common_Style.Common_BackgroundColor}>
                <View style={Common_Style.Common_Header_View}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={Common_Style.Common_BackTouch} >
                        <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../../assets/images/White_BackArrow.png')}
                                style={Common_Style.Common_Header_Icon} />
                        </View>
                    </TouchableOpacity>
                    <Title style={{ fontFamily: 'Segoe UI' }}>Notifications</Title>
                </View>
                <Content>
                    <ScrollView>
                        {this.state.no_data != '' ?
                            <Card style={Common_Style.Notification_Card}>
                                <CardItem style={{ backgroundColor: Color.common_White, alignSelf: 'center' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>{this.state.no_data}</Text>
                                </CardItem>
                            </Card> :
                            <View>
                                {this.state.is_loading ?
                                    <Loading />
                                    :
                                    <Card style={Common_Style.Notification_Card}>
                                        {typeof this.state.Notification_Data != undefined && typeof this.state.Notification_Data != null && this.state.Notification_Data.length != 0 ?
                                            this.state.Notification_Data.map((data, index) => {
                                                return (
                                                    <CardItem style={Common_Style.Notification_CardItem}>
                                                        <Body>
                                                            <View style={{ flexDirection: 'row' }}>
                                                                <Image source={require('../../assets/images/dot.png')}
                                                                    style={Common_Style.Notification_DotIcon} />
                                                                <View style={{ paddingLeft: '3%' }}>
                                                                    <ViewMoreText
                                                                        numberOfLines={1}
                                                                        renderViewMore={this.renderViewMore}
                                                                        renderViewLess={this.renderViewLess}
                                                                        textStyle={{ marginRight: '6%', fontFamily: 'Segoe UI', fontSize: Color.common_Small }}
                                                                        key={data.id}
                                                                    >
                                                                        <Text style={Common_Style.Notification_CardItem_Txt} >{data.message}</Text>
                                                                    </ViewMoreText>
                                                                    <View style={{ flexDirection: 'row', marginTop: '3%' }}>
                                                                        <Text style={Common_Style.Notification_CardItem_Txt}>{moment(data.created_date).format('L')},</Text>
                                                                        <Text style={Common_Style.Notification_CardItem_Txt2}>{moment(data.created_date).format('LT')}</Text>
                                                                    </View>
                                                                </View>
                                                            </View>
                                                        </Body>
                                                    </CardItem>
                                                )
                                            }) : null}
                                    </Card>
                                }
                            </View>
                        }
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}