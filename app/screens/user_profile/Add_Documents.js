import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Container, Content, Text, Title } from 'native-base';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import DocumentPicker from 'react-native-document-picker';
import Color from '../../assets/colors/colors';
import Common_Style from '../../assets/styles/style';
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Add_Document extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            is_Btnloading: false,
            User_id: this.props.navigation.getParam('User_id'),
            Add_Get_doc: '',
            Add_Doc_Uploaded: '',
            doc_details: [],
            prev_doc_details: [],
            new_doc_details: [],
            Down_Arrow: true,
            is_loading: true,
            added_doc_length: '',
        };
    }

    componentWillMount() {
        Params.Add_doc.user_id = this.state.User_id,
            Api.Post_Vendor_Add_Get_Doc(Params.Add_doc, this.Add_Doc_Response, this.Add_Doc_Error)
    }

    Add_Doc_Response = (response) => {
        if (response.status == 'success') {
            this.setState({
                doc_details: response.result,
                is_loading: false,
                added_doc_length: response.result.length
            })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    Add_Doc_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    async  Attachments() {
        try {
            const results = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            for (const res of results) {
                if (this.state.added_doc_length <= 4) {
                    this.state.new_doc_details.push(res)
                    this.setState({
                        new_doc_details: this.state.new_doc_details,
                        added_doc_length: this.state.added_doc_length + 1
                    })
                } else {
                    ToastMsg('danger', 'Select maximum five document', Color.toast_danger)
                }
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }
    remove_Document = (index) => {
        var array = [...this.state.new_doc_details];
        array.splice(index, 1);
        this.setState({ new_doc_details: array });
    }

    Upload_all_documents = () => {
        if (this.state.new_doc_details != '') {
            Upload_Data = new FormData();
            this.state.new_doc_details.map((data, index) => {
                Upload_Data.append('uploaded_file[]', {
                    uri: data.uri,
                    type: data.type,
                    name: data.name,
                });
            })
            Api.Post_Vendor_Add_Doc_Upload(Upload_Data, this.Add_Doc_Upload_Response, this.Add_Doc_Upload_Error)
            this.setState({ is_Btnloading: true })
        } else (
            ToastMsg('danger', 'Select minimum one document', Color.toast_danger),
            this.setState({ is_Btnloading: false })
        )
    }

    Add_Doc_Upload_Response = async (response) => {
        if (response.status == 'success') {
            this.setState({
                Add_Doc_Uploaded: response.result[0],
            })
            this.Add_Doc_Update()
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Btnloading: false })
        }
    }

    Add_Doc_Upload_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_Btnloading: false })
    }

    Add_Doc_Update() {
        switch (this.state.doc_details.length) {
            case 0:
                Params.Add_Doc_Update.user_id = this.state.User_id,
                    Params.Add_Doc_Update.image0 = this.state.Add_Doc_Uploaded.image0,
                    Params.Add_Doc_Update.image1 = this.state.Add_Doc_Uploaded.image1,
                    Params.Add_Doc_Update.image2 = this.state.Add_Doc_Uploaded.image2,
                    Params.Add_Doc_Update.image3 = this.state.Add_Doc_Uploaded.image3,
                    Params.Add_Doc_Update.image4 = this.state.Add_Doc_Uploaded.image4
                break;
            case 1:
                Params.Add_Doc_Update.user_id = this.state.User_id,
                    Params.Add_Doc_Update.image0 = this.state.doc_details[0].image0,
                    Params.Add_Doc_Update.image1 = this.state.Add_Doc_Uploaded.image0,
                    Params.Add_Doc_Update.image2 = this.state.Add_Doc_Uploaded.image1,
                    Params.Add_Doc_Update.image3 = this.state.Add_Doc_Uploaded.image2,
                    Params.Add_Doc_Update.image4 = this.state.Add_Doc_Uploaded.image3
                break;
            case 2:
                Params.Add_Doc_Update.user_id = this.state.User_id,
                    Params.Add_Doc_Update.image0 = this.state.doc_details[0].image0,
                    Params.Add_Doc_Update.image1 = this.state.doc_details[1].image1,
                    Params.Add_Doc_Update.image2 = this.state.Add_Doc_Uploaded.image0,
                    Params.Add_Doc_Update.image3 = this.state.Add_Doc_Uploaded.image1,
                    Params.Add_Doc_Update.image4 = this.state.Add_Doc_Uploaded.image2
                break;
            case 3:
                Params.Add_Doc_Update.user_id = this.state.User_id,
                    Params.Add_Doc_Update.image0 = this.state.doc_details[0].image0,
                    Params.Add_Doc_Update.image1 = this.state.doc_details[1].image1,
                    Params.Add_Doc_Update.image2 = this.state.doc_details[2].image2,
                    Params.Add_Doc_Update.image3 = this.state.Add_Doc_Uploaded.image0,
                    Params.Add_Doc_Update.image4 = this.state.Add_Doc_Uploaded.image1
                break;
            case 4:
                Params.Add_Doc_Update.user_id = this.state.User_id,
                    Params.Add_Doc_Update.image0 = this.state.doc_details[0].image0,
                    Params.Add_Doc_Update.image1 = this.state.doc_details[1].image1,
                    Params.Add_Doc_Update.image2 = this.state.doc_details[2].image2,
                    Params.Add_Doc_Update.image3 = this.state.doc_details[3].image3,
                    Params.Add_Doc_Update.image4 = this.state.Add_Doc_Uploaded.image0
                break;
        }
        Api.Post_Vendor_Add_Doc_Update(Params.Add_Doc_Update, this.Add_Doc_Update_Response, this.Add_Doc_Update_Error)
        this.setState({ is_Btnloading: true })
    }

    Add_Doc_Update_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ is_Btnloading: false })
            this.props.navigation.navigate('Dashboard')
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Btnloading: false })
        }
    }

    Add_Doc_Update_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_Btnloading: false })
    }

    Up_Down_Arrow = () => {
        this.setState({ Down_Arrow: !this.state.Down_Arrow })
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <View style={Common_Style.Common_Header_View}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={Common_Style.Common_BackTouch}>
                        <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../../assets/images/White_BackArrow.png')}
                                style={Common_Style.Common_Header_Icon} />
                        </View>
                    </TouchableOpacity>
                    <Title style={{ fontFamily: 'Segoe UI' }}>Add Documents</Title>
                </View>
                <Content>
                    <ScrollView keyboardShouldPersistTaps='on-drag' >
                        {this.state.is_loading ? <Loading /> :
                            <View style={{ marginTop: '15%', width: '100%' }}>
                                <View style={{ width: '90%', alignSelf: 'center', backgroundColor: Color.common_White, borderRadius: 10 }}>
                                    <View style={{ flexDirection: 'row', paddingVertical: '3%' }}>
                                        <View style={{ width: '90%' }}>
                                            <TouchableOpacity onPress={() => { this.Attachments() }} >
                                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                    <Image source={require('../../assets/images/Upload.png')} style={{ height: 20, width: 20, marginHorizontal: '5%' }} />
                                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI Bold' }}>Upload Document</Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        <TouchableOpacity onPress={() => { this.Up_Down_Arrow() }}>
                                            <Image source={(this.state.Down_Arrow) ?
                                                require('../../assets/images/Down.png') :
                                                require('../../assets/images/Up.png')}
                                                style={{ height: 20, width: 20 }}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                    {(this.state.Down_Arrow == true) ? null :
                                        <View style={{ paddingLeft: '3%' }}>
                                            <View style={{ flexDirection: 'row', marginTop: '3%' }}>
                                                <View style={{ paddingHorizontal: '3%' }}>
                                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>1 </Text>
                                                </View>
                                                <View>
                                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>To make sure that you are autenticated user,</Text>
                                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>Upload your certified registered service pdf/excel/png document.</Text>
                                                </View>
                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: '3%', marginBottom: '3%' }}>
                                                <View style={{ paddingHorizontal: '3%' }}>
                                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>2</Text>
                                                </View>
                                                <View>
                                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>To Know your identity,</Text>
                                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>Upload Passport photo/Adhar card photo/Voter Id/Pan card photo.</Text>
                                                </View>
                                            </View>
                                        </View>}
                                </View>
                                <View style={{ marginTop: '5%' }}>
                                    {this.state.new_doc_details.map((data, index) => {
                                        return (
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Text style={{ fontSize: Color.common_Medium, fontFamily: 'Segoe UI Bold', color: Color.common_White, marginLeft: '5%', paddingVertical: '2%' }}>
                                                    {data.name}
                                                </Text>
                                                <TouchableOpacity onPress={() => { this.remove_Document(index) }}>
                                                    <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={require('../../assets/images/cancel.png')} style={{ backgroundColor: Color.common_Black, borderRadius: 10, height: 15, width: 15 }} />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    })}
                                </View>
                                <View style={{ marginTop: '5%', marginBottom: '5%' }}>
                                    {this.state.is_Btnloading ? <Loading /> :
                                        <TouchableOpacity
                                            style={{ borderWidth: 1, borderColor: Color.common_Green, borderRadius: 5, width: '90%', height: 45, alignSelf: 'center', backgroundColor: Color.common_Green, justifyContent: 'center' }}
                                            onPress={() => { this.Upload_all_documents() }} >
                                            <Text style={{ textAlign: 'center', color: Color.common_White, fontFamily: 'Segoe UI' }}>Submit</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        }
                    </ScrollView>
                </Content>
            </Container >
        )
    }
}