import React, { Component } from 'react';
import { View, ScrollView, TouchableOpacity, Image, TextInput, ImageBackground, AsyncStorage, RefreshControl } from 'react-native';
import { Container, Content, Text, Button, Title } from 'native-base';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';
import ImagePicker from 'react-native-image-picker';
import Modal from "react-native-modal";

export default class User_Profile extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(prop) {
        super(prop);
        this.state = {
            user_id: '',
            user_info: '',
            user_name: '',
            mobile: '',
            email: '',
            apartment_name: '',
            Pincode: '',
            address: '',
            is_valid_name: true,
            is_valid_email: true,
            editable: false,
            editableIcon: false,
            user_details: '',
            is_loading: true,
            is_Imgloading: false,
            is_Uploading: false,
            updated_user_info: '',
            avatarSource: require('../../assets/images/default_profile.png'),
            isModalVisible: false,
            is_Refreshing: false,
            image_id: '',
            img1: "",
            img2: "",
            img3: "",
            img4: "",
            img5: "",
        }
    }

    async  componentWillMount() {
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        Params.User_Details.user_id = this.state.user_details[0].user_id;
        Api.Post_UsersDetails(Params.User_Details, this.HandleUsersDetails_Response, this.HandleUsersDetails_Error)
        this.setState({ is_loading: true })
    }

    HandleUsersDetails_Response = (response) => {
        if (response.status == 'success') {
            this.setState({
                user_info: response.response,
                img1: response.response[0].image_0,
                img2: response.response[0].image_1,
                img3: response.response[0].image_2,
                img4: response.response[0].image_3,
                img5: response.response[0].image_4,
                user_id: response.response[0].user_id,
                user_image_id: response.response[0].user_image_id,
                user_name: response.response[0].user_name,
                mobile: response.response[0].mobile,
                email: response.response[0].email,
                apartment_name: response.response[0].apartment_name,
                Pincode: response.response[0].Pincode,
                address: response.response[0].address,
                Uploaded_Documents: response.response[0].image_0,
                is_loading: false
            })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleUsersDetails_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    Edit_Profile_Details = () => {
        this.setState({
            editable: true,
            editableIcon: true
        })
    }

    Profile_ImagePicker = () => {
        const options = {
            title: 'Select Avatar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };
        ImagePicker.showImagePicker(options, (response) => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                const source = { uri: response.uri };
                this.setState({
                    avatarSource: source,
                });
                const ImageData = new FormData();
                ImageData.append('uploaded_file', {
                    uri: response.uri,
                    type: response.type,
                    name: response.fileName
                });
                this.setState({ is_Imgloading: true })
                Api.Post_UserImageUpload(ImageData, this.HandleProfileImageUpload_Response, this.HandleProfileImageUpload_Error)
            }
        });
    }

    HandleProfileImageUpload_Response = (response) => {
        if (response.status == 'success') {
            Params.ProfileimageUpdate.user_id = this.state.user_details[0].user_id,
                Params.ProfileimageUpdate.user_image_name = response.result;
            Api.Post_ProfileimageUpdate(Params.ProfileimageUpdate, this.HandleProfileimageUpdate_Response, this.HandleProfileimageUpdate_Error)
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Imgloading: false })
        }
    }

    HandleProfileImageUpload_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_Imgloading: false })
    }

    HandleProfileimageUpdate_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({
                is_Imgloading: false
            })
            Params.User_Details.user_id = this.state.user_details[0].user_id;
            Api.Post_UsersDetails(Params.User_Details, this.EditDetails_Response, this.EditUsersDetails_Error)
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Imgloading: false })
        }
    }

    EditDetails_Response = (response) => {
        if (response.status == 'success') {
            this.setState({
                user_info: response.response,
                user_name: response.response[0].user_name,
                mobile: response.response[0].mobile,
                email: response.response[0].email,
                apartment_name: response.response[0].apartment_name,
                Pincode: response.response[0].Pincode,
                address: response.response[0].address,
                is_loading: false,
                updated_user_info: response.response,
            })
            this.UpdatedUserDetails(this.state.updated_user_info)
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    EditUsersDetails_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }
    HandleProfileimageUpdate_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_Imgloading: false })
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    Handle_UpdateProfile = () => {
        if (this.state.name != "") {
            this.setState({
                is_valid_name: true,
                is_valid_email: true,
            })
            Params.Edit_Profile.user_ids = this.state.user_details[0].user_id,
                Params.Edit_Profile.user_names = this.state.user_name,
                Api.Post_EditProfile(Params.Edit_Profile, this.HandleEditProfile_Response, this.HandleEditProfile_Error)
            this.setState({ is_Uploading: true })
        } else {
            this.validateInputFields(this.state.name, 'is_valid_name');
            this.validateInputFields(this.state.email_id, 'is_valid_email');
        }
    }

    HandleEditProfile_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({
                is_Uploading: false,
                updated_user_info: response.response
            })
            this.UpdatedUserDetails(this.state.updated_user_info)
            Params.User_Details.user_id = this.state.user_details[0].user_id;
            Api.Post_UsersDetails(Params.User_Details, this.HandleUsersDetails_Response, this.HandleUsersDetails_Error)
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Uploading: false })
        }
    }

    HandleEditProfile_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    UpdatedUserDetails = async (data) => {
        try {
            await AsyncStorage.setItem('VENDOR_DETAILS', JSON.stringify(data));
        } catch (error) {
        }
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    toggleModal0 = (image_id) => {
        this.setState({ isModalVisible: !this.state.isModalVisible, img: this.state.img1, image_id: image_id });
    };

    toggleModal1 = (image_id) => {
        this.setState({ isModalVisible: !this.state.isModalVisible, img: this.state.img2, image_id: image_id });
    };

    toggleModal2 = (image_id) => {
        this.setState({ isModalVisible: !this.state.isModalVisible, img: this.state.img3, image_id: image_id });
    };

    toggleModal3 = (image_id) => {
        this.setState({ isModalVisible: !this.state.isModalVisible, img: this.state.img4, image_id: image_id });
    };

    toggleModal4 = (image_id) => {
        this.setState({ isModalVisible: !this.state.isModalVisible, img: this.state.img5, image_id: image_id });
    };

    Edit_Document(image_id) {
        this.toggleModal(false)
        this.props.navigation.navigate('Edit_Documents', { User_id: this.state.user_id, image_id: image_id })
    }

    Add_Documents() {
        this.props.navigation.navigate('Add_Documents', { User_id: this.state.user_id })
    }

    _onRefresh() {
        setTimeout(() => {
            this.componentWillMount()
        }, 100);
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                {typeof this.state.user_info != undefined && typeof this.state.user_info != null && this.state.user_info.length != 0 ?
                    this.state.user_info
                        .map((data, index) => {
                            return (
                                <View style={Common_Style.Profile_Header}>
                                    <View style={Common_Style.Profile_ToBack_View}>
                                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                            <View style={{ flexDirection: 'row', marginLeft: '10%', alignItems: 'center' }}>
                                                <Image source={require('../../assets/images/White_BackArrow.png')}
                                                    style={Common_Style.Profile_BackIcon} />
                                                <Title style={{ fontFamily: 'Segoe UI', marginLeft: '5%' }}>My Profile</Title>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={Common_Style.Profile_Header_Center}>
                                        <ImageBackground
                                            source={data.user_image_id == null ? this.state.avatarSource : { uri: data.user_image_id }}
                                            style={Common_Style.MyProfile_ProfileImg}
                                            imageStyle={{ borderRadius: 60 }}
                                        >
                                            {this.state.is_Imgloading || this.state.is_loading ? <Loading /> :
                                                <View style={Common_Style.MyProfile_ImgEdit_View}>
                                                    <TouchableOpacity onPress={() => { this.Profile_ImagePicker() }}>
                                                        <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                            <Image source={require('../../assets/images/camera.png')} style={{ height: 25, width: 25 }} />
                                                        </View>
                                                    </TouchableOpacity >
                                                </View>}
                                        </ImageBackground>
                                        <Text style={Common_Style.Profile_Name} >{data.user_name}</Text>
                                        <Text style={Common_Style.Profile_MobNo} > {data.mobile}</Text>
                                    </View>
                                    <View style={Common_Style.Profile_EditIcon_View}>
                                        <TouchableOpacity onPress={() => { this.Edit_Profile_Details() }}>
                                            <View style={{ width: 30, height: 30, alignItems: 'center', justifyContent: 'center' }}>
                                                <Image source={(!this.state.editableIcon) ?
                                                    require('../../assets/images/edit.png') :
                                                    null}
                                                    style={Common_Style.Profile_EditIcon} />
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        }) :
                    <View style={Common_Style.Common_Header_View}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={Common_Style.Common_BackTouch}>
                            <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                                <Image source={require('../../assets/images/White_BackArrow.png')}
                                    style={Common_Style.Common_Header_Icon} />
                            </View>
                        </TouchableOpacity>
                        <Title style={{ fontFamily: 'Segoe UI' }}>My Profile</Title>
                    </View>
                }
                <Modal
                    animationType="fade"
                    transparent={true}
                    isVisible={this.state.isModalVisible}
                    backdropColor={'#000'}
                    backdropOpacity={0.25}
                >
                    <View style={Common_Style.Model_FullView}>
                        <View style={Common_Style.Model_View}>
                            <View style={{ height: 400 }}>
                                <Image
                                    source={{ uri: this.state.img }}
                                    style={{ height: '100%', width: '100%' }}
                                />
                            </View>
                            <View style={Common_Style.Model_BottomBorder}>
                                <View style={Common_Style.Doc_Model_BottomLableView}>
                                    <TouchableOpacity onPress={() => { this.toggleModal(false) }}>
                                        <View style={{ width: 100, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={Common_Style.Doc_Model_BottomLableTxt}>CANCEL</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => { this.Edit_Document(this.state.image_id) }}>
                                        <View style={{ width: 100, height: 50, justifyContent: 'center', alignItems: 'center' }}>
                                            <Text style={Common_Style.Doc_Model_BottomLableTxt}>EDIT</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </View>
                </Modal>
                {this.state.is_loading ? <Loading /> :
                    <Content refreshControl={
                        <RefreshControl
                            refreshing={this.state.is_Refreshing}
                            onRefresh={this._onRefresh.bind(this)}
                            title="Loading..."
                            progressBackgroundColor={'#E67E22'}
                        />
                    }>
                        <ScrollView>
                            <View style={{ marginBottom: '10%' }}>
                                <View>
                                    <Text style={Common_Style.Profile_Lable}>
                                        Name
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            value={this.state.user_name}
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.setState({ user_name: text })}
                                            selectionColor={'#000'}
                                            editable={this.state.editable}
                                            placeholder={'Enter name'}
                                        />
                                    </View>
                                    {this.state.is_valid_name == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Name
                                    </Text> : null}
                                </View>
                                <View>
                                    <Text style={Common_Style.Profile_Lable}>
                                        Mobile number
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            value={this.state.mobile}
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.setState({ mobile: text })}
                                            selectionColor={'#000'}
                                            editable={false}
                                            placeholder={'Enter mobile number'} />
                                    </View>
                                </View>
                                <View>
                                    <Text style={Common_Style.Profile_Lable}>
                                        Pin Code
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            value={this.state.Pincode}
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.setState({ PinCode: text })}
                                            selectionColor={'#000'}
                                            editable={false}
                                            placeholder={'Enter PinCode'}
                                        />
                                    </View>
                                </View>
                                <View>
                                    <Text style={Common_Style.Profile_Lable}>
                                        Address
                                    </Text>
                                    <View style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '90%', height: 110, alignSelf: 'center', backgroundColor: Color.common_White }}  >
                                        <TextInput
                                            value={this.state.address}
                                            style={{ color: Color.common_Black, fontFamily: 'Segoe UI', fontSize: Color.common_Small, width: '90%', paddingLeft: '3%', textAlignVertical: 'top' }}
                                            onChangeText={(text) => this.setState({ Address: text })}
                                            placeholder={'Enter Address'}
                                            numberOfLines={2}
                                            multiline={true}
                                            editable={false}
                                        />
                                    </View>
                                </View>
                                {(this.state.editable == false) ? null :
                                    <View style={{ marginTop: '15%' }}>
                                        {this.state.is_Uploading ? <Loading /> :
                                            <Button
                                                style={Common_Style.Profile_Update_Btn}
                                                onPress={() => { this.Handle_UpdateProfile() }}
                                            >
                                                <Text style={Common_Style.Profile_Update_Txt}>UPDATE</Text>
                                            </Button>
                                        }
                                    </View>
                                }
                                <View style={{ borderBottomWidth: 1, borderBottomColor: Color.common_Black, marginTop: '5%' }} />
                                <View>
                                    <Text style={Common_Style.Profile_Lable}>
                                        Uploaded Documents
                                    </Text>
                                    {this.state.img1 != "null" ?
                                        <View style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '90%', alignSelf: 'center', backgroundColor: Color.common_White, marginTop: '2%' }}  >
                                            <View style={{ marginTop: '2%', marginBottom: '2%', marginLeft: '3%' }}>
                                                <TouchableOpacity onPress={() => { this.toggleModal0("image0") }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={require('../../assets/images/document.png')} style={{ height: 20, width: 20 }} />
                                                        <Text style={{ marginLeft: '1%', fontSize: Color.common_XSmall }}>View Document 1</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View> : null}
                                    {this.state.img2 != "null" ?
                                        <View style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '90%', alignSelf: 'center', backgroundColor: Color.common_White, marginTop: '5%' }}  >
                                            <View style={{ marginTop: '2%', marginBottom: '2%', marginLeft: '3%' }}>
                                                <TouchableOpacity onPress={() => { this.toggleModal1("image1") }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={require('../../assets/images/document.png')} style={{ height: 20, width: 20 }} />
                                                        <Text style={{ marginLeft: '1%', fontSize: Color.common_XSmall }}>View Document 2</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        : null}
                                    {this.state.img3 != "null" ?
                                        <View style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '90%', alignSelf: 'center', backgroundColor: Color.common_White, marginTop: '5%' }}  >
                                            <View style={{ marginTop: '2%', marginBottom: '2%', marginLeft: '3%' }}>
                                                <TouchableOpacity onPress={() => { this.toggleModal2("image2") }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={require('../../assets/images/document.png')} style={{ height: 20, width: 20 }} />
                                                        <Text style={{ marginLeft: '1%', fontSize: Color.common_XSmall }}>View Document 3</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        : null}
                                    {this.state.img4 != "null" ?
                                        <View style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '90%', alignSelf: 'center', backgroundColor: Color.common_White, marginTop: '5%' }}  >
                                            <View style={{ marginTop: '2%', marginBottom: '2%', marginLeft: '3%' }}>
                                                <TouchableOpacity onPress={() => { this.toggleModal3("image3") }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={require('../../assets/images/document.png')} style={{ height: 20, width: 20 }} />
                                                        <Text style={{ marginLeft: '1%', fontSize: Color.common_XSmall }}>View Document 4</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        : null}
                                    {this.state.img5 != "null" ?
                                        <View style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '90%', alignSelf: 'center', backgroundColor: Color.common_White, marginTop: '5%' }}  >
                                            <View style={{ marginTop: '2%', marginBottom: '2%', marginLeft: '3%' }}>
                                                <TouchableOpacity onPress={() => { this.toggleModal4("image4") }}>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                        <Image source={require('../../assets/images/document.png')} style={{ height: 20, width: 20 }} />
                                                        <Text style={{ marginLeft: '1%', fontSize: Color.common_XSmall }}>View Document 5</Text>
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        : null}
                                    {this.state.img1 == "null" || this.state.img2 == "null" || this.state.img3 == "null" || this.state.img4 == "null" || this.state.img5 == "null" ?
                                        <View style={{ marginTop: '5%' }}>
                                            <Button
                                                onPress={() => { this.Add_Documents() }}
                                                style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '30%', height: 30, alignSelf: 'center', backgroundColor: Color.common_White, justifyContent: 'center' }}>
                                                <Text style={{ textAlign: 'center', color: Color.common_Black, fontFamily: 'Segoe UI' }}>Add+</Text>
                                            </Button>
                                        </View> :
                                        <View style={{ marginTop: '5%' }}>
                                            <Button
                                                disabled={true}
                                                onPress={() => { this.Add_Documents() }}
                                                style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '30%', height: 30, alignSelf: 'center', backgroundColor: Color.common_White, justifyContent: 'center', opacity: 0.2 }}>
                                                <Text style={{ textAlign: 'center', color: Color.common_Black, fontFamily: 'Segoe UI' }}>Add+</Text>
                                            </Button>
                                        </View>
                                    }
                                </View>
                            </View>
                        </ScrollView>
                    </Content>
                }
            </Container>
        )
    }
}
