import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Text } from 'native-base';
import OTPInput from 'react-native-otp';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Loading from '../../assets/loading/Loading';
import { ToastMsg } from '../../assets/helper/Helper';

export default class OTP extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            otp: '',
            is_loading: false,
            is_valied_otp: true,
            mobile_no: this.props.navigation.getParam('mobile_No'),
            Screen: this.props.navigation.getParam('screen')
        };
    }

    Register_Verify_otp = () => {
        if (this.state.otp.length < 4) {
            this.setState({ is_valied_otp: false })
        } else {
            this.setState({ is_loading: true, is_valied_otp: true })
            switch (this.state.Screen) {
                case 'REGISTER':
                    Params.Register_VerifyOTP.mobileno = this.state.mobile_no,
                        Params.Register_VerifyOTP.otp = this.state.otp
                    Api.Post_RegisterVerify_OTP(Params.Register_VerifyOTP, this.HandleRegisterOTP_Response, this.HandleRegisterOTP_Error)
                    break;
                case 'FORGOTPASSWORD':
                    Params.Forgot_OTP.email_or_mobile = this.state.mobile_no,
                        Params.Forgot_OTP.otp = this.state.otp
                    Api.Post_ForgotVerify_Otp(Params.Forgot_OTP, this.HandleForgotOTP_Response, this.HandleForgotOTP_Error)
                    break;
            }
        }
    }

    HandleRegisterOTP_Response = async (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ is_loading: false })
            let TOKEN = await AsyncStorage.getItem('Vendor_fcmToken');
            Params.InsertToken.user_id = response.response[0].user_id,
                Params.InsertToken.token = TOKEN;
            Api.Post_Insert_Token(Params.InsertToken, this.HandleToken_Response, this.HandleToken_Error)
            this.props.navigation.navigate('Upload_Documents', { User_id: response.response[0].user_id })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleToken_Response = (response) => {
        if (response.status == 'success') {
            this.setState({ is_loading: false })
        } else {
            this.setState({ is_loading: false })
        }
    }

    HandleToken_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    HandleRegisterOTP_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    HandleForgotOTP_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ is_loading: false })
            this.props.navigation.navigate('Reset_Password', { mobileNo: this.state.mobile_no })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleForgotOTP_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    Resend_otp = () => {
        this.setState({ is_loading: true })
        Params.ResendOTP.mobile = this.state.mobile_no
        Api.Post_Resend_otp(Params.ResendOTP, this.Resend_otp_Response, this.Resend_otp_Error)
    }

    Resend_otp_Response = (response) => {
        this.setState({ is_loading: false })
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    Resend_otp_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <Content >
                    <ScrollView>
                        <View>
                            <View>
                                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                    <View style={{ height: 60, width: 60, justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../../assets/images/Black_BackArrow.png')}
                                            style={Common_Style.ForgotPwd_BackIcon}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Image
                                    source={require('../../assets/images/Logo196.png')}
                                    style={Common_Style.OTP_img}
                                />
                            </View>
                            <View style={Common_Style.ForgotPwd_CardView}>
                                <Text style={Common_Style.ForgotPwd_Txt}>Verification Code</Text>
                                <View style={{ marginTop: '15%' }}>
                                    <Text style={Common_Style.OTP_Txt} >Please Enter Verification Code Which is send to your registered phone no.</Text>
                                    <OTPInput
                                        value={this.state.otp}
                                        onChange={(otp) => this.setState({ otp: otp })}
                                        tintColor='transparent'
                                        offTintColor='transparent'
                                        otpLength={4}
                                        autoFocus
                                        cellStyle={Common_Style.OTP_CellSty}
                                    />
                                </View>
                                {this.state.is_valied_otp == false ?
                                    <Text style={Common_Style.OTPError_Txt}>
                                        Please Enter 4 Digit OTP
                                    </Text> : null}
                                <View style={{ marginTop: '25%', marginBottom: '5%' }}>
                                    {this.state.is_loading ? <Loading /> :
                                        <TouchableOpacity style={Common_Style.ForgotPwd_Btn_View} onPress={() => { this.Register_Verify_otp() }} >
                                            {this.state.Screen == 'REGISTER' ?
                                                <Text style={Common_Style.ForgotPwd_Btn_Txt}> NEXT</Text> :
                                                <Text style={Common_Style.ForgotPwd_Btn_Txt}> SUBMIT</Text>}
                                        </TouchableOpacity>
                                    }
                                </View>
                                <View style={Common_Style.ResendOTP_View}>
                                    <TouchableOpacity onPress={() => { this.Resend_otp() }}>
                                        <Text style={Common_Style.ResendOTP_Txt} >Resend OTP?</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}