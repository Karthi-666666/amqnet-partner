import React, { Component } from 'react';
import { View, Image, TouchableOpacity, BackHandler, Alert } from 'react-native';
import { Container, Tab, Tabs, Title } from 'native-base';
import Color from '../../assets/colors/colors'
import New_Requests from '../Dashboard/New_Requests';
import Accepted_Requests from '../Dashboard/Accepted_Requests';
import Dashboard_Style from '../../assets/styles/style';
import { DrawerActions } from 'react-navigation-drawer';

export default class Vendor_Dashboard extends Component {

  static navigationOptions = {
    header: null
  }

  constructor() {
    super();
    this.state = {
      isLoading: false,
    }
  }

  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }
  backPressed = () => {
    if (this.props.navigation.isFocused()) {
      Alert.alert(
        "AMQNET",
        "Do you want to exit?",
        [
          {
            text: "No",
            onPress: () => "Cancel Pressed",
            style: "cancel"
          },
          { text: "Yes", onPress: () => BackHandler.exitApp() }
        ],
        { cancelable: false }
      );
      return true
    }

  }
  DrawerOpen() {
    this.props.navigation.dispatch(DrawerActions.toggleDrawer())
  }
  Notifications = () => {
    this.props.navigation.navigate('Notifications')
  }

  render() {
    return (
      <Container>
        <View style={Dashboard_Style.Dashboard_HeaderView}>
          <TouchableOpacity onPress={() => { this.DrawerOpen() }} activeOpacity={2} >
            <View style={{ height: '100%', width: 50, alignItems: 'center', justifyContent: 'center' }}>
              <Image
                source={require('../../assets/images/menu.png')}
                style={{ width: 20, height: 20 }}
              />
            </View>
          </TouchableOpacity >
          <Title style={{ width: '60%', fontFamily: 'Segoe UI' }}>Complaints</Title>
          <TouchableOpacity onPress={() => { this.Notifications() }} >
            <View style={{ height: '100%', width: 50, alignItems: 'center', justifyContent: 'center' }}>
              <Image
                source={require('../../assets/images/notification.png')}
                style={{ width: 25, height: 25, borderRadius: 50 }}
              />
            </View>
          </TouchableOpacity>
        </View>
        <Tabs tabBarUnderlineStyle={{ backgroundColor: Color.common_Green }}>
          <Tab
            heading="New Requests"
            tabStyle={{ backgroundColor: Color.common_White }}
            textStyle={{ color: Color.common_Green, fontSize: Color.common_Medium, textAlign: "center" }}
            activeTabStyle={{ backgroundColor: Color.common_White }}
            inactiveTextStyle={{ color: Color.common_Green, fontWeight: 'normal', fontSize: Color.common_Medium, fontFamily: 'Segoe UI' }}
            activeTextStyle={{ color: Color.common_Green, fontWeight: 'normal', fontSize: Color.common_Medium, fontFamily: 'Segoe UI Bold', textAlign: "center" }}>
            <New_Requests />
          </Tab>
          <Tab
            heading="Accepted Requests"
            tabStyle={{ backgroundColor: Color.common_White }}
            textStyle={{ color: Color.common_Green, fontSize: Color.common_Medium, textAlign: "center" }}
            activeTabStyle={{ backgroundColor: Color.common_White }}
            inactiveTextStyle={{ color: Color.common_Green, fontWeight: 'normal', fontSize: Color.common_Medium, fontFamily: 'Segoe UI' }}
            activeTextStyle={{ color: Color.common_Green, fontWeight: 'normal', fontSize: Color.common_Medium, fontFamily: 'Segoe UI Bold', textAlign: "center" }}>
            <Accepted_Requests data={this.props.navigation} />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}