import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ScrollView, AsyncStorage, Linking, Image, RefreshControl } from 'react-native';
import { Content, Card, CardItem, Body } from 'native-base'
import Dashboard_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Loading from '../../assets/loading/Loading';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import moment from 'moment';

export default class New_Requests extends Component {

  static navigationOptions = {
    header: null,
  }

  constructor() {
    super();
    this.state = {
      Pending_List: [],
      is_loading: true,
      no_data: '',
      is_Refreshing: false,
    }
  }

  async componentWillMount() {
    this.setState({ is_loading: true })
    let value = await AsyncStorage.getItem('VENDOR_DETAILS');
    this.setState({ user_details: JSON.parse(value) });
    Params.Vendor_Task_Pending_List.user_id = this.state.user_details[0].user_id,
      Api.Post_Vendor_task_pending_list(Params.Vendor_Task_Pending_List, this.HandleVendor_Task_Pending_List_Response, this.HandleVendor_Task_Pending_List_Error)
  }

  HandleVendor_Task_Pending_List_Response = (response) => {
    if (response.status == 'success') {
      this.setState({
        Pending_List: response.response,
        is_loading: false
      })
    } else {
      this.setState({ is_loading: false, no_data: response.message, Pending_List: '' })
    }
  }

  HandleVendor_Task_Pending_List_Error = (error) => {
    ToastMsg('danger', error, Color.toast_danger)
    this.setState({ is_loading: false })
  }

  async VendorTask_Accept(issue_id) {
    let value = await AsyncStorage.getItem('VENDOR_DETAILS');
    this.setState({ user_details: JSON.parse(value) });
    Params.VendorTask_Accept.user_id = this.state.user_details[0].user_id,
      Params.VendorTask_Accept.complaint_id = issue_id,
      Api.Post_Vendor_TaskAccept(Params.VendorTask_Accept, this.HandlePost_Vendor_TaskAccept_Response, this.HandlePost_Vendor_TaskAccept_Error)
  }

  HandlePost_Vendor_TaskAccept_Response = (response) => {
    if (response.status == 'success') {
      ToastMsg('success', response.message, Color.toast_success)
      this.setState({ is_loading: false })
      Params.Vendor_Task_Pending_List.user_id = this.state.user_details[0].user_id,
        Api.Post_Vendor_task_pending_list(Params.Vendor_Task_Pending_List, this.HandleVendor_Task_Pending_List_Response, this.HandleVendor_Task_Pending_List_Error)
    } else {
      this.setState({ is_loading: false, no_data: response.message })
    }
  }

  HandlePost_Vendor_TaskAccept_Error = (error) => {
    ToastMsg('danger', error, Color.toast_danger)
    this.setState({ is_loading: false })
  }

  convertTimeOnly = (time) => {
    var time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
      time = time.slice(1);
      var ampm = time[0] < 12 ? 'AM' : 'PM';
      var hr = time[0] % 12 || 12;
    }
    var str = hr + ':' + time[2] + ':' + ampm;
    return str;
  }

  _onRefresh() {
    setTimeout(() => {
      this.componentWillMount()
    }, 100);
  }

  render() {
    return (
      <Content style={{ backgroundColor: Color.common_gray }} refreshControl={
        <RefreshControl
          refreshing={this.state.is_Refreshing}
          onRefresh={this._onRefresh.bind(this)}
          title="Loading..."
          progressBackgroundColor={'#E67E22'}
        />
      }>
        <ScrollView>
          {this.state.is_loading ? <Loading /> :
            <View style={{ marginBottom: '5%', marginTop: '5%', }}>
              {typeof this.state.Pending_List != 'undefined' && typeof this.state.Pending_List != null && this.state.Pending_List.length != 0 && this.state.Pending_List != '' ?
                this.state.Pending_List.map((data, index) => {
                  return (
                    <View>
                      <View style={Dashboard_Style.Dashboard_Card_FullView}>
                        <Card style={{ width: '90%' }}>
                          <CardItem>
                            <Body>
                              <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '50%' }}>
                                  <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, fontFamily: 'Segoe UI Bold' }}>Issue #{data.issue_number}</Text>
                                  <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, marginVertical: '3%', fontFamily: 'Segoe UI' }}>{data.issue_name}</Text>
                                  <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI Bold', width: '80%' }} >{data.service_type}</Text>
                                    {data.service_type == "Service Later" ?
                                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{moment(data.service_later_date).format('L')},</Text>
                                        <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI', marginLeft: '2%' }}>{this.convertTimeOnly(data.service_later_time)},</Text>
                                      </View> : null}
                                  </View>
                                  <View style={{ flexDirection: 'row', marginTop: '3%', alignItems: 'center' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '80%' }} >Amount</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>₹ {data.amount}</Text>
                                  </View>
                                  <View style={{ flexDirection: 'row', marginTop: '3%', alignItems: 'center' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '80%' }} >Created On</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                      <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{moment(data.issue_raised_date).format('L')},</Text>
                                      <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI', marginLeft: '3%' }}>{moment(data.issue_raised_date).format('LT')},</Text>
                                    </View>
                                  </View>
                                  <View style={{ flexDirection: 'row', marginTop: '3%', alignItems: 'center' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '80%' }} >Apartment Name</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{data.apartment_name}</Text>
                                  </View>
                                  <View style={{ flexDirection: 'row', marginTop: '3%' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '80%' }} >Address</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, fontFamily: 'Segoe UI' }}>{data.apartment_address}</Text>
                                  </View>
                                  <View style={{ flexDirection: 'row', marginTop: '3%', alignItems: 'center' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '80%' }} >Pincode</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{data.pincode}</Text>
                                  </View>
                                  <View style={{ flexDirection: 'row', marginTop: '3%', alignItems: 'center' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '80%' }} >Contact Number</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{data.security_phone}</Text>
                                    <TouchableOpacity onPress={() => { Linking.openURL(`tel:${data.security_phone}`); }} style={{ marginLeft: '10%' }} >
                                      <Image
                                        source={require('../../assets/images/call.png')}
                                        style={{ width: 15, height: 15 }} />
                                    </TouchableOpacity>
                                  </View>
                                </View>
                                <View style={{ marginLeft: '15%' }}>
                                  <TouchableOpacity onPress={() => { this.VendorTask_Accept(data.issue_number) }}>
                                    <View style={{ width: 70, height: 25, borderColor: Color.common_Green, borderWidth: 1, borderRadius: 5, alignItems: 'center', justifyContent: 'center' }}>
                                      <Text style={{ fontSize: Color.common_XSmall, color: Color.common_Green, fontFamily: 'Segoe UI' }}>Accept</Text>
                                    </View>
                                  </TouchableOpacity>
                                </View>
                              </View>
                              {data.service_type == "Service Later" ?
                                <View style={{ marginTop: '3%' }}>
                                  <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: Color.common_Large, fontFamily: 'Segoe UI', marginRight: '2%', color: Color.common_Red }} >**</Text>
                                    <Text style={{ fontSize: Color.common_XSmall, color: Color.Txt_Gray, fontFamily: 'Segoe UI', width: '90%' }}>For Service Later, Make sure you are reaching your customer on specified time and date else you will be charged for delay.</Text>
                                  </View>
                                </View> :
                                <View style={{ marginTop: '3%' }}>
                                  <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: Color.common_Large, fontFamily: 'Segoe UI', color: Color.common_Red, marginRight: '2%' }} >**</Text>
                                    <Text style={{ fontSize: Color.common_XSmall, color: Color.Txt_Gray, fontFamily: 'Segoe UI', width: '90%' }}>Make sure you are reaching your customer within 1 hour else you will charged for delay.</Text>
                                  </View>
                                </View>
                              }
                            </Body>
                          </CardItem>
                        </Card>
                      </View>
                    </View>
                  )
                }) :
                <View style={{ alignSelf: 'center', marginTop: '20%' }}>
                  <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>{this.state.no_data}</Text>
                </View>
              }
            </View>
          }
        </ScrollView>
      </Content>
    )
  }
}