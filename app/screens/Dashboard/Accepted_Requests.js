import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView, AsyncStorage, Linking, RefreshControl } from 'react-native';
import { Content, Card, CardItem, Body } from 'native-base'
import Dashboard_Style from '../../assets/styles/style';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Loading from '../../assets/loading/Loading';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import moment from 'moment';
import Modal from "react-native-modal";
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';

export default class Accepted_Requests extends Component {

  static navigationOptions = {
    header: null,
  }

  constructor() {
    super();
    this.state = {
      Accepted_List: [],
      Cancel_Data: [],
      is_loading: true,
      no_data: '',
      isModalVisible: false,
      is_Refreshing: false,
    }
  }

  async  componentWillMount() {
    this.setState({ is_loading: true })
    let value = await AsyncStorage.getItem('VENDOR_DETAILS');
    this.setState({ user_details: JSON.parse(value) });
    Params.Vendor_Task_Accepted_List.user_id = this.state.user_details[0].user_id,
      Api.Post_Vendor_task_accepted_list(Params.Vendor_Task_Accepted_List, this.HandleVendor_Task_Accepted_List_Response, this.HandleVendor_Task_Accepted_List_Error)
  }

  HandleVendor_Task_Accepted_List_Response = (response) => {
    if (response.status == 'success') {
      this.setState({
        Accepted_List: response.response,
        is_loading: false
      })
    } else {
      this.setState({ is_loading: false, no_data: response.message, })
    }
  }

  HandleVendor_Task_Accepted_List_Error = (error) => {
    ToastMsg('danger', error, Color.toast_danger)
    this.setState({ is_loading: false })
  }

  componentDidMount() {
    Api.get_Vendor_task_cancel_reasons(this.CancelWork_Response, this.CancelWork_Error)
  }

  toggleModal = (status, issue_id) => {
    this.setState({
      isModalVisible: !this.state.isModalVisible,
      complaint_id: issue_id
    });
  };

  onSelect(index, value) {
    this.setState({
      text: `Selected index: ${index} , value: ${value}`
    })
  }

  Submit_VendorTask_Cancel() {
    Params.Vendor_Task_Cancel.reason = this.state.Cancel_Data[0].id,
      Params.Vendor_Task_Cancel.user_id = this.state.user_details[0].user_id,
      Params.Vendor_Task_Cancel.complaint_id = this.state.complaint_id,
      Api.Post_Vendor_Task_Cancel(Params.Vendor_Task_Cancel, this.Vendor_Task_Cancel_Response, this.Vendor_Task_Cancel_Error)
  }

  Vendor_Task_Cancel_Response = (response) => {
    if (response.status == 'success') {
      ToastMsg('success', response.message, Color.toast_success)
      this.setState({ is_loading: false })
      this.toggleModal(false)
    } else {
      ToastMsg('danger', response.message, Color.toast_danger)
      this.setState({ is_loading: false })
    }
  }

  Vendor_Task_Cancel_Error = (error) => {
    ToastMsg('danger', error, Color.toast_danger)
    this.setState({ is_loading: false })
  }

  Complaint_Details(complain_info) {
    this.props.data.navigate('Complaint_Details', { Accepted_List_Data: complain_info })
  }

  CancelWork_Response = (response) => {
    if (response.status == 'success') {
      this.setState({ Cancel_Data: response.response })
    } else {
      ToastMsg('danger', response.message, Color.toast_danger)
    }
  }

  CancelWork_Error = (error) => {
    ToastMsg('danger', error, Color.toast_danger)
    this.setState({ is_loading: false })
  }

  convertTimeOnly = (time) => {
    var time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
    if (time.length > 1) {
      time = time.slice(1);
      var ampm = time[0] < 12 ? 'AM' : 'PM';
      var hr = time[0] % 12 || 12;
    }
    var str = hr + ':' + time[2] + ':' + ampm;
    return str;
  }

  _onRefresh() {
    setTimeout(() => {
      this.componentWillMount()
    }, 100);
  }

  render() {
    return (
      <Content style={{ backgroundColor: Color.common_gray }} refreshControl={
        <RefreshControl
          refreshing={this.state.is_Refreshing}
          onRefresh={this._onRefresh.bind(this)}
          title="Loading..."
          progressBackgroundColor={'#E67E22'}
        />
      }>
        <Modal
          animationType="fade"
          transparent={true}
          isVisible={this.state.isModalVisible}
          backdropColor={'#000'}
          backdropOpacity={0.25}
        >
          <View style={{ backgroundColor: Color.common_White, borderRadius: 10 }}>
            <View style={{ backgroundColor: Color.common_Green, borderColor: Color.common_Green, borderWidth: 1, height: 40, justifyContent: 'center' }}>
              <Text style={{ fontSize: Color.common_Medium, color: Color.common_White, fontFamily: 'Segoe UI Bold', marginLeft: '15%' }}>Cancel Request</Text>
            </View>
            <View style={{ marginLeft: '10%', marginTop: '5%' }}>
              <RadioGroup
                size={24}
                thickness={2}
                color={Color.common_Green}
                highlightColor={Color.common_White}
                selectedIndex={0}
                onSelect={(index, value) => this.onSelect(index, value)}
              >
                {this.state.Cancel_Data.map((data, index) => {
                  return (
                    <RadioButton value={data.id}
                      color={Color.common_Green} >
                      <Text>{data.reasons}</Text>
                    </RadioButton>
                  )
                })}
              </RadioGroup>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-evenly', marginBottom: '5%', marginTop: '5%' }}>
              <TouchableOpacity onPress={() => { this.toggleModal(false, null) }}>
                <View style={{ borderWidth: 1, borderColor: Color.common_Green, borderRadius: 5, width: 100, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontSize: Color.common_Medium, color: Color.common_Red, fontFamily: 'Segoe UI' }}>Cancel</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.Submit_VendorTask_Cancel()}>
                <View style={{ borderWidth: 1, borderColor: Color.common_Green, borderRadius: 5, width: 100, height: 40, backgroundColor: Color.common_Green, alignItems: 'center', justifyContent: 'center' }}>
                  <Text style={{ fontSize: Color.common_Medium, color: Color.common_White, fontFamily: 'Segoe UI' }}>Submit</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
        <ScrollView>
          {this.state.is_loading ? <Loading /> :
            <View style={{ marginBottom: '5%', marginTop: '5%', }}>
              {typeof this.state.Accepted_List != undefined && typeof this.state.Accepted_List != null && this.state.Accepted_List.length != 0 ?
                this.state.Accepted_List.map((data, index) => {
                  return (
                    <View>
                      <View style={Dashboard_Style.Dashboard_Card_FullView}>
                        <Card style={{ width: '90%' }}>
                          <CardItem>
                            <Body>
                              <View style={{ flexDirection: 'row' }}>
                                <View style={{ width: '80%' }}>
                                  <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, fontFamily: 'Segoe UI Bold' }}>Issue #{data.issue_number}</Text>
                                  <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, marginVertical: '3%', fontFamily: 'Segoe UI' }}>{data.issue_name}</Text>
                                  <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI Bold', width: '60%' }} >{data.service_type}</Text>
                                    {data.service_type == "Service Later" ?
                                      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{moment(data.service_later_date).format('L')},</Text>
                                        <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI', marginLeft: '2%' }}>{this.convertTimeOnly(data.service_later_time)},</Text>
                                      </View> : null}
                                  </View>
                                  <View style={Common_Style.Complaint_TxtView}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '60%' }} >Amount</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>₹ {data.amount}</Text>
                                  </View>
                                  <View style={Common_Style.Complaint_TxtView}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '60%' }} >Created On</Text>
                                    <View style={{ flexDirection: 'row' }}>
                                      <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{moment(data.issue_raised_date).format('L')},</Text>
                                      <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI', marginLeft: '3%' }}>{moment(data.issue_raised_date).format('LT')},</Text>
                                    </View>
                                  </View>
                                  <View style={Common_Style.Complaint_TxtView}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '60%' }} >Apartment Name</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{data.apartment_name}</Text>
                                  </View>
                                  <View style={{ flexDirection: 'row', marginTop: '3%' }}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '60%' }} >Address</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, fontFamily: 'Segoe UI', width: '60%' }}>{data.apartment_address}</Text>
                                  </View>
                                  <View style={Common_Style.Complaint_TxtView}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '60%' }} >Pincode</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{data.pincode}</Text>
                                  </View>
                                  <View style={Common_Style.Complaint_TxtView}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '60%' }} >Contact Number</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>{data.security_phone}</Text>
                                  </View>
                                  <View style={Common_Style.Complaint_TxtView}>
                                    <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '60%' }} >Status</Text>
                                    <Text style={{ fontSize: Color.common_Small, color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>Accepted</Text>
                                  </View>
                                </View>
                                <View>
                                </View>
                              </View>
                              {data.service_type == "Service Later" ?
                                <View style={{ marginTop: '3%' }}>
                                  <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontSize: Color.common_Large, fontFamily: 'Segoe UI Italic', marginRight: '2%', color: Color.common_Red }} >**</Text>
                                    <Text style={{ fontSize: Color.common_XSmall, color: Color.Txt_Gray, fontFamily: 'Segoe UI', width: '90%' }}>For Service Later, Make sure you are reaching your customer on specified time and date else you will be charged for delay.</Text>
                                  </View>
                                </View> :
                                <View style={{ marginTop: '3%' }}>
                                  <View style={{ flexDirection: 'row', }}>
                                    <Text style={{ fontSize: Color.common_Large, fontFamily: 'Segoe UI', color: Color.common_Red, marginRight: '2%' }} >**</Text>
                                    <Text style={{ fontSize: Color.common_XSmall, color: Color.Txt_Gray, fontFamily: 'Segoe UI', width: '90%' }}>Make sure you are reaching your customer within 1 hour else you will charged for delay.</Text>
                                  </View>
                                </View>}
                              <View style={{ flexDirection: 'row', marginTop: '5%', alignSelf: 'center' }}>
                                <TouchableOpacity onPress={() => { Linking.openURL(`tel:${data.security_phone}`); }}>
                                  <View style={{ flexDirection: 'row', borderColor: Color.common_Green, borderWidth: 1, borderRadius: 5, height: 30, width: 80, justifyContent: 'space-evenly', alignItems: 'center' }}>
                                    <Image
                                      source={require('../../assets/images/call.png')}
                                      style={{ width: 20, height: 20 }} />
                                    <Text style={{ color: Color.common_Green, fontSize: Color.common_Small, fontFamily: 'Segoe UI', }}>Call</Text>
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity style={{ width: "100%" }} onPress={() => { this.Complaint_Details(data) }} style={{ marginHorizontal: '5%' }}>
                                  <View style={{ backgroundColor: Color.common_Green, borderColor: Color.common_Green, borderWidth: 1, borderRadius: 5, height: 30, width: 80, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: Color.common_White, fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>View</Text>
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => { this.toggleModal(true, data.issue_number) }} >
                                  <View style={{ borderColor: Color.common_Green, borderWidth: 1, borderRadius: 5, height: 30, width: "100%", justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ color: Color.common_Red, fontSize: Color.common_Small, fontFamily: 'Segoe UI', marginLeft: 10, marginRight: 10 }}>Cancel Request</Text>
                                  </View>
                                </TouchableOpacity>
                              </View>
                            </Body>
                          </CardItem>
                        </Card>
                      </View>
                    </View>
                  )
                }) :
                <View style={{ alignSelf: 'center', marginTop: '20%' }}>
                  <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>{this.state.no_data}</Text>
                </View>
              }
            </View>
          }
        </ScrollView>
      </Content>
    )
  }
}