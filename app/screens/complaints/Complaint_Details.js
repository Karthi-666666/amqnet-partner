import React, { Component } from 'react';
import { TextInput, View, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Text, Button, Title, Body, Card, CardItem, } from 'native-base';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Modal from "react-native-modal";
import Loading from '../../assets/loading/Loading';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';

export default class Complaint_Details extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            Accepted_List_Data: this.props.navigation.getParam('Accepted_List_Data'),
            Vendor_Viewdetails_Data: '',
            isLoading: false,
            otp: '',
            Extra_Amout: '',
            user_details: '',
            isModalVisible: false,
            Ex_loading: false,
            Verified_OTP: '',
            Work_Stered: '',
            Work_Ended: '',
            is_valid_Extra_Amout: true,
            QuitTask: ''
        };
    }

    async componentWillMount() {
        this.setState({ isLoading: true })
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        Params.Vendor_accepted_complaints_viewdetails.user_id = this.state.user_details[0].user_id,
            Params.Vendor_accepted_complaints_viewdetails.complaint_id = this.state.Accepted_List_Data.issue_number,
            Api.Post_Vendor_Accepted_Complaints_Viewdetails(Params.Vendor_accepted_complaints_viewdetails, this.Vendor_accepted_complaints_viewdetails_Response, this.Vendor_accepted_complaints_viewdetails_Error)
    }

    Vendor_accepted_complaints_viewdetails_Response = (response) => {
        if (response.status == 'success') {
            this.setState({ Vendor_Viewdetails_Data: response.response, isLoading: false })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ isLoading: false })
        }
    }

    Vendor_accepted_complaints_viewdetails_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ isLoading: false })
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    async Handle_ExtraAmout() {
        if (this.state.Extra_Amout.trim() != "") {
            this.setState({
                is_valid_Extra_Amout: true,
            })
            let value = await AsyncStorage.getItem('VENDOR_DETAILS');
            this.setState({ user_details: JSON.parse(value) });
            Params.Vendor_Extra_amount_request.user_id = this.state.user_details[0].user_id,
                Params.Vendor_Extra_amount_request.complaint_id = this.state.Accepted_List_Data.issue_number,
                Params.Vendor_Extra_amount_request.apartment_id = this.state.Accepted_List_Data.apartment_id,
                Params.Vendor_Extra_amount_request.extra_amount = this.state.Extra_Amout
            Api.Post_Vendor_extra_amount_request(Params.Vendor_Extra_amount_request, this.ExtraAmout_Response, this.ExtraAmout_Error)
            this.setState({ Ex_loading: true })
        } else {
            this.validateInputFields(this.state.Extra_Amout, 'is_valid_Extra_Amout');
        }
    }

    ExtraAmout_Response = (response) => {
        if (response.status == 'success') {
            this.toggleModal(false)
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({
                ExtraAmout_Data: response.response,
                Ex_loading: false
            })
            this.componentWillMount()
            this.textamount.clear()
        } else {
            this.setState({ Ex_loading: false, no_data: response.message, })
        }
    }

    ExtraAmout_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ Ex_loading: false })
    }
    async VendorsVerify_OTP(text) {
        this.setState({ otp: text })
        if (text.length == 4) {
            let value = await AsyncStorage.getItem('VENDOR_DETAILS');
            this.setState({ user_details: JSON.parse(value) });
            Params.Vendors_Verify_OTP.user_id = this.state.user_details[0].user_id,
                Params.Vendors_Verify_OTP.complaint_id = this.state.Accepted_List_Data.issue_number,
                Params.Vendors_Verify_OTP.task_otp = this.state.otp,
                Api.Post_VendorsVerify_OTP(Params.Vendors_Verify_OTP, this.Vendors_Verify_OTP_Response, this.Vendors_Verify_OTP_Error)
        }
    }

    Vendors_Verify_OTP_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.componentWillMount()
            this.setState({ Verified_OTP: response.status })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
        }
    }

    Vendors_Verify_OTP_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ loading: false })
    }

    async Start_Work() {
        this.setState({ isLoading: true })
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        Params.Vendor_Start_Work.user_id = this.state.user_details[0].user_id,
            Params.Vendor_Start_Work.complaint_id = this.state.Accepted_List_Data.issue_number,
            Api.Post_Vendor_start_work(Params.Vendor_Start_Work, this.Vendor_Start_Work_Response, this.Vendor_Start_Work_Error)
    }

    Vendor_Start_Work_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ Work_Stered: response.task_status, isLoading: false })
            this.componentWillMount()
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ isLoading: false })
        }
    }

    Vendor_Start_Work_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ isLoading: false })
    }

    async End_Work() {
        this.setState({ isLoading: true })
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        Params.Vendor_end_work.user_id = this.state.user_details[0].user_id,
            Params.Vendor_end_work.complaint_id = this.state.Accepted_List_Data.issue_number,
            Api.Post_Vendor_end_work(Params.Vendor_end_work, this.Vendor_end_work_Response, this.Vendor_end_work_Error)
    }

    Vendor_end_work_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ Work_Ended: response.task_status, isLoading: false })
            this.props.navigation.navigate('Dashboard')
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ isLoading: false })
        }
    }

    Vendor_end_work_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ isLoading: false })
    }

    async Vendor_QuitTask() {
        this.setState({ isLoading: true })
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        Params.Vendor_quit_task.user_id = this.state.user_details[0].user_id,
            Params.Vendor_quit_task.complaint_id = this.state.Accepted_List_Data.issue_number,
            Api.Post_Vendor_Quit_Task(Params.Vendor_quit_task, this.Vendor_QuitTask_Response, this.Vendor_QuitTask_Error)
    }

    Vendor_QuitTask_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ QuitTask: response.task_status, isLoading: false })
            this.props.navigation.navigate('Dashboard')
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ isLoading: false })
        }
    }

    Vendor_QuitTask_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ isLoading: false })
    }

    toggleModal = () => {
        this.setState({ isModalVisible: !this.state.isModalVisible });
    };

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <View style={Common_Style.Common_Header_View}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <View style={{ width: 50, height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../../assets/images/White_BackArrow.png')}
                                style={Common_Style.Common_Header_Icon} />
                        </View>
                    </TouchableOpacity>
                    <Title style={{ fontFamily: 'Segoe UI' }}>Complaint details</Title>
                </View>
                <Modal
                    animationType="fade"
                    transparent={true}
                    isVisible={this.state.isModalVisible}
                    backdropColor={'#000'}
                    backdropOpacity={0.25}
                >
                    <View style={Common_Style.Model_FullView}>
                        {this.state.Ex_loading ? <Loading /> :
                            <View style={Common_Style.Model_View}>
                                <View style={Common_Style.Model_LableTxtView} >
                                    <Text style={{ paddingVertical: '3%', color: '#636359', fontFamily: 'Segoe UI', fontSize: Color.common_Small }}>Enter the Amount</Text>
                                    <TextInput
                                        ref={input => { this.textamount = input }}
                                        keyboardType="number-pad"
                                        onChangeText={(text) => this.setState({ Extra_Amout: text })}
                                        style={Common_Style.Txt_Input}
                                        style={Common_Style.Model_TxtInput}
                                    />
                                    {this.state.is_valid_Extra_Amout == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Extra Amount
                                    </Text> : null}
                                </View>
                                <View style={Common_Style.Model_BottomBorder}>
                                    <View style={Common_Style.Model_BottomLableView}>
                                        <TouchableOpacity onPress={() => { this.toggleModal(false) }}>
                                            <Text style={[Common_Style.Model_BottomLableTxt, { marginRight: '5%' }]}>CANCEL</Text>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => { this.Handle_ExtraAmout() }} >
                                            <Text style={Common_Style.Model_BottomLableTxt}>REQUEST</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        }
                    </View>
                </Modal>
                <Content>
                    <ScrollView>
                        {this.state.isLoading ? <Loading /> :
                            <View style={Common_Style.Complaint_Card_FullView}>
                                <Card style={Common_Style.Complaint_CardView}>
                                    <CardItem>
                                        <Body>
                                            <View style={{ flexDirection: 'row' }}>
                                                <View style={{ marginLeft: '5%' }}>
                                                    <Text style={Common_Style.Complaint_IssueNo_Txt}>Issue #{this.state.Vendor_Viewdetails_Data.issue_number}</Text>
                                                    <View style={Common_Style.Complaint_TxtView}>
                                                        <Text style={Common_Style.Complaint_Txt1} >Service Type:</Text>
                                                        <Text style={Common_Style.Complaint_Txt2}>{this.state.Vendor_Viewdetails_Data.service_type}</Text>
                                                    </View>
                                                    <View style={Common_Style.Complaint_TxtView}>
                                                        <Text style={Common_Style.Complaint_Txt1} >Issue Name:</Text>
                                                        <Text style={Common_Style.Complaint_Txt2}>{this.state.Vendor_Viewdetails_Data.issue_name}</Text>
                                                    </View>
                                                    <View style={Common_Style.Complaint_TxtView}>
                                                        <Text style={Common_Style.Complaint_Txt1} >Fees:</Text>
                                                        <Text style={Common_Style.Complaint_Txt2}>₹ {this.state.Vendor_Viewdetails_Data.amount}</Text>
                                                    </View>
                                                    {this.state.Vendor_Viewdetails_Data.extra_amount_status == 1 ?
                                                        <View>
                                                            <View style={Common_Style.Complaint_TxtView}>
                                                                <Text style={Common_Style.Complaint_Txt1} >Extra Amount:</Text>
                                                                <Text style={Common_Style.Complaint_Txt2}>₹ {this.state.Vendor_Viewdetails_Data.extra_amount}</Text>
                                                            </View>
                                                            <View style={Common_Style.Complaint_TxtView}>
                                                                <Text style={Common_Style.Complaint_Txt1} >Total Amount:</Text>
                                                                <Text style={Common_Style.Complaint_Txt2}>₹ {this.state.Vendor_Viewdetails_Data.total_amount}</Text>
                                                            </View>
                                                        </View> : null
                                                    }
                                                    <View style={Common_Style.Complaint_TxtView}>
                                                        <Text style={Common_Style.Complaint_Txt1} >Apartment Name:</Text>
                                                        <Text style={Common_Style.Complaint_Txt2}>{this.state.Vendor_Viewdetails_Data.apartment_name}</Text>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', marginTop: '3%' }}>
                                                        <Text style={Common_Style.Complaint_Txt1} >Address:</Text>
                                                        <Text style={[Common_Style.Complaint_Txt2, { width: '50%' }]}>{this.state.Vendor_Viewdetails_Data.apartment_address}</Text>
                                                    </View>
                                                    <View style={Common_Style.Complaint_TxtView}>
                                                        <Text style={Common_Style.Complaint_Txt1} >Pincode:</Text>
                                                        <Text style={Common_Style.Complaint_Txt2}>{this.state.Vendor_Viewdetails_Data.pincode}</Text>
                                                    </View>
                                                    <View style={Common_Style.Complaint_TxtView}>
                                                        <Text style={Common_Style.Complaint_Txt1} >Contact Number:</Text>
                                                        <Text style={Common_Style.Complaint_Txt2}>{this.state.Vendor_Viewdetails_Data.security_phone}</Text>
                                                    </View>
                                                    {this.state.Vendor_Viewdetails_Data.otp_status == 0 ?
                                                        <View style={Common_Style.Complaint_TxtView}>
                                                            <Text style={Common_Style.Complaint_Txt1} >OTP:</Text>
                                                            <TextInput
                                                                placeholder={'- - - - '}
                                                                onChangeText={(text) => this.VendorsVerify_OTP(text)}
                                                                style={Common_Style.Complaint_Txt2}
                                                                maxLength={4}
                                                                keyboardType="number-pad"
                                                            />
                                                        </View> : null}
                                                    {this.state.Vendor_Viewdetails_Data.otp_status == 1 ?
                                                        <View style={Common_Style.Complaint_TxtView}>
                                                            <Text style={Common_Style.Complaint_Txt1} >OTP:</Text>
                                                            <Text style={Common_Style.Complaint_Txt2}>{this.state.Vendor_Viewdetails_Data.task_otp}</Text>
                                                        </View> : null}
                                                </View>
                                            </View>
                                            <View style={{ marginTop: '20%', marginBottom: '1%', alignSelf: 'center' }}>
                                                {this.state.Vendor_Viewdetails_Data.task_status == 0 ?
                                                    <View>
                                                        {this.state.Vendor_Viewdetails_Data.otp_status == 1 ?
                                                            <View>
                                                                <Button style={Common_Style.Complaint_details_BtnView}
                                                                    onPress={() => { this.Start_Work() }}
                                                                >
                                                                    <Text uppercase={false} style={Common_Style.Complaint_details_BtnTxt}>Start Work</Text>
                                                                </Button>
                                                            </View> :
                                                            <View>
                                                                <Button style={Common_Style.Complaint_details_DBtnView}
                                                                    disabled={true}
                                                                    onPress={() => { this.Start_Work() }}
                                                                >
                                                                    <Text uppercase={false} style={Common_Style.Complaint_details_BtnTxt}>Start Work</Text>
                                                                </Button>
                                                            </View>
                                                        }
                                                        {this.state.Vendor_Viewdetails_Data.extra_amount_status == 1 ?
                                                            <View style={{ marginTop: '5%' }}>
                                                                <Button style={Common_Style.Complaint_details_Disable_RqBtn}
                                                                    disabled={true}
                                                                    onPress={() => { this.toggleModal(true) }}>
                                                                    <Text uppercase={false} style={Common_Style.Complaint_details_RqBtnTxt}>Request Extra amount</Text>
                                                                </Button>
                                                            </View>
                                                            :
                                                            <View style={{ marginTop: '5%' }}>
                                                                <Button style={Common_Style.Complaint_details_RqBtnView}
                                                                    onPress={() => { this.toggleModal(true) }}>
                                                                    <Text uppercase={false} style={Common_Style.Complaint_details_RqBtnTxt}>Request Extra amount</Text>
                                                                </Button>
                                                            </View>}
                                                    </View>
                                                    : null}
                                                {this.state.Vendor_Viewdetails_Data.task_status == 1 ?
                                                    <View>
                                                        <Button style={Common_Style.Complaint_details_BtnView}
                                                            onPress={() => { this.End_Work() }}>
                                                            <Text uppercase={false} style={Common_Style.Complaint_details_BtnTxt}>End Work</Text>
                                                        </Button>
                                                        {this.state.Vendor_Viewdetails_Data.extra_amount_status == 1 ?
                                                            <View style={{ marginTop: '5%' }}>
                                                                <Button style={Common_Style.Complaint_details_Disable_RqBtn}
                                                                    disabled={true}
                                                                    onPress={() => { this.toggleModal(true) }}>
                                                                    <Text uppercase={false} style={Common_Style.Complaint_details_RqBtnTxt}>Request Extra amount</Text>
                                                                </Button>
                                                            </View>
                                                            :
                                                            <View style={{ marginTop: '5%' }}>
                                                                <Button style={Common_Style.Complaint_details_RqBtnView}
                                                                    onPress={() => { this.toggleModal(true) }}>
                                                                    <Text uppercase={false} style={Common_Style.Complaint_details_RqBtnTxt}>Request Extra amount</Text>
                                                                </Button>
                                                            </View>}
                                                    </View>
                                                    : null}
                                                {this.state.Vendor_Viewdetails_Data.task_status == 2 ?
                                                    <View style={{ alignSelf: 'center', marginBottom: '10%' }}>
                                                        <Text style={{ fontSize: Color.common_XSmall, color: Color.Txt_Gray, fontFamily: 'Segoe UI', textAlign: 'center' }} >"wait for process you will receive your payment shortly"</Text>
                                                    </View> : null
                                                }
                                                {this.state.Vendor_Viewdetails_Data.task_status == 2 || this.state.Vendor_Viewdetails_Data.task_status == 3 || this.state.Vendor_Viewdetails_Data.task_status == 4 ? null :
                                                    <View style={{ marginTop: '5%' }}>
                                                        <TouchableOpacity onPress={() => { this.Vendor_QuitTask() }}  >
                                                            <Text style={{ color: Color.Txt_Gray, textAlign: 'center', fontFamily: 'Segoe UI' }}>Quit</Text>
                                                        </TouchableOpacity>
                                                    </View>
                                                }
                                                {this.state.Vendor_Viewdetails_Data.task_status == 3 ?
                                                    <View style={{ alignSelf: 'center', marginBottom: '10%' }}>
                                                        <Text style={{ fontSize: Color.common_XSmall, color: Color.Txt_Gray, fontFamily: 'Segoe UI' }} >"Quited this Task"</Text>
                                                    </View> : null
                                                }
                                                {this.state.Vendor_Viewdetails_Data.task_status == 4 ?
                                                    <View style={{ alignSelf: 'center', marginBottom: '10%' }}>
                                                        <Text style={{ fontSize: Color.common_XSmall, color: Color.Txt_Gray, fontFamily: 'Segoe UI' }} >"Task Completed"</Text>
                                                    </View> : null
                                                }
                                            </View>
                                        </Body>
                                    </CardItem>
                                </Card>
                            </View>
                        }
                    </ScrollView>
                </Content>
            </Container >
        )
    }
}