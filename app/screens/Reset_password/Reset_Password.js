import React, { Component } from 'react';
import { TextInput, View, Image, TouchableOpacity, ScrollView, BackHandler, Alert } from 'react-native';
import { Container, Content, Text } from 'native-base';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Reset_Password extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            mobile_no: this.props.navigation.getParam('mobileNo'),
            userpassword: '',
            Confirm_Password: '',
            hidePassword: true,
            Confirm_hidePassword: true,
            is_valid_userpassword: true,
            is_valid_Confirm_Password: true,
            pass_validation: true
        };
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    }

    backPressed = () => {
        if (this.props.navigation.isFocused()) {
            Alert.alert(
                "AMQNET",
                "Do you want to exit?",
                [
                    {
                        text: "No",
                        onPress: () => "Cancel Pressed",
                        style: "cancel"
                    },
                    { text: "Yes", onPress: () => BackHandler.exitApp() }
                ],
                { cancelable: false }
            );
            return true
        }
    }

    validatePassword = (text) => {
        this.setState({
            pass_validation: text.length <= 7 ? false : true,
            userpassword: text,
            is_valid_userpassword: true
        })
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    Handle_ResetPass = () => {
        if (this.state.userpassword.trim() != "" && this.state.Confirm_Password.trim() != "" && this.state.pass_validation == true) {
            this.setState({ is_valid_userpassword: true, is_valid_Confirm_Password: true })
            if (this.state.userpassword != this.state.Confirm_Password) {
                ToastMsg('danger', 'Password Mismatched!', Color.toast_danger)
            } else {
                Params.ResetPassword.email_or_mobile = this.state.mobile_no,
                    Params.ResetPassword.new_password = this.state.userpassword,
                    Params.ResetPassword.confirm_password = this.state.Confirm_Password
                Api.Post_ResetPassword(Params.ResetPassword, this.HandleResetPassword_Response, this.HandleResetPassword_Error)
                this.setState({ is_loading: true })
                this.props.navigation.navigate('Login')
            }
        }
        else {
            this.validateInputFields(this.state.userpassword, 'is_valid_userpassword');
            this.validateInputFields(this.state.Confirm_Password, 'is_valid_Confirm_Password');
        }
    }

    HandleResetPassword_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ is_loading: false })
            this.props.navigation.navigate('Login')
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleResetPassword_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    PasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    Re_PasswordVisibility = () => {
        this.setState({ Confirm_hidePassword: !this.state.Confirm_hidePassword });
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <Content>
                    <ScrollView keyboardShouldPersistTaps='always'>
                        <View>
                            <View>
                                <Image
                                    source={require('../../assets/images/Logo196.png')}
                                    style={Common_Style.ChangePwd_img}
                                />
                            </View>
                            <View style={Common_Style.ChangePwd_CardView}>
                                <View style={{ marginTop: '10%' }} >
                                    <Text style={Common_Style.Lable_Txt}>
                                        New Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={require('../../assets/images/password.png')}
                                                style={{ width: 20, height: 20, marginHorizontal: '2%' }} />
                                            <TextInput
                                                value={this.state.userpassword}
                                                onChangeText={(text) => this.validatePassword(text)}
                                                secureTextEntry={this.state.hidePassword}
                                                style={Common_Style.Resend_InField}
                                                selectionColor={'#000'}
                                                placeholder={'New Password'}
                                            />
                                            <View style={{ alignSelf: 'center' }}>
                                                <TouchableOpacity onPress={() => this.PasswordVisibility()} >
                                                    <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={(this.state.hidePassword) ?
                                                            require('../../assets/images/hidePass.png') :
                                                            require('../../assets/images/SHowPass.png')}
                                                            style={Common_Style.eye_Icon} />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                {this.state.is_valid_userpassword == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Enter Password
                                    </Text> : null}
                                {this.state.pass_validation == false && this.state.is_valid_userpassword == true ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Password must contain atleast 8 characters
                                    </Text> : null}
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Confirm Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={require('../../assets/images/password.png')}
                                                style={{ width: 20, height: 20, marginHorizontal: '2%' }} />
                                            <TextInput
                                                value={this.state.Confirm_Password}
                                                onChangeText={(text) => this.setState({ Confirm_Password: text })}
                                                secureTextEntry={this.state.Confirm_hidePassword}
                                                style={Common_Style.Resend_InField}
                                                selectionColor={'#000'}
                                                placeholder={'Confirm Password'}
                                            />
                                            <View>
                                                <TouchableOpacity onPress={() => this.Re_PasswordVisibility()} >
                                                    <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={(this.state.Confirm_hidePassword) ?
                                                            require('../../assets/images/hidePass.png') :
                                                            require('../../assets/images/SHowPass.png')}
                                                            style={Common_Style.eye_Icon} />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                {this.state.is_valid_Confirm_Password == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Re-enter  Password
                                    </Text> : null}
                                <View style={{ marginTop: '10%', marginBottom: '20%' }}>
                                    {this.state.is_loading ? <Loading /> :
                                        <TouchableOpacity style={Common_Style.ForgotPwd_Btn_View} onPress={() => { this.Handle_ResetPass() }} >
                                            <Text style={Common_Style.ForgotPwd_Btn_Txt}>Save</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}