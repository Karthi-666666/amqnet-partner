import React, { Component } from 'react';
import { TextInput, View, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Text, Title, } from 'native-base';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Change_Password extends Component {

    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
            user_id: '',
            Old_password: '',
            New_Password: '',
            Confirm_Password: '',
            Old_hidePassword: true,
            New_hidePassword: true,
            Confirm_hidePassword: true,
            is_valid_OldPass: true,
            is_valid_NewPass: true,
            is_valid_ConPass: true,
            pass_validation: true
        };
    }

    async  componentWillMount() {
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        this.setState({
            user_id: this.state.user_details[0].user_id
        })
    }

    Old_PasswordVisibility = () => {
        this.setState({ Old_hidePassword: !this.state.Old_hidePassword });
    }

    New_PasswordVisibility = () => {
        this.setState({ New_hidePassword: !this.state.New_hidePassword });
    }

    Confirm_PasswordVisibility = () => {
        this.setState({ Confirm_hidePassword: !this.state.Confirm_hidePassword });
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    validatePassword = (text) => {
        this.setState({
            pass_validation: text.length <= 7 ? false : true,
            New_Password: text,
            is_valid_NewPass: true
        })
    }

    Handle_ChangePass = () => {
        if (this.state.Old_password.trim() != "" && this.state.New_Password.trim() != "" && this.state.Confirm_Password.trim() != "" && this.state.pass_validation == true) {
            this.setState({ is_valid_OldPass: true, is_valid_NewPass: true, is_valid_ConPass: true })

            if (this.state.New_Password != this.state.Confirm_Password) {
                ToastMsg('danger', 'Password mismatched!', Color.toast_danger)
            } else {
                Params.ChangePassword.user_id = this.state.user_id,
                    Params.ChangePassword.old_password = this.state.Old_password,
                    Params.ChangePassword.new_password = this.state.New_Password
                Api.Post_Changepassword(Params.ChangePassword, this.HandleChangePassword_Response, this.HandleChangePassword_Error)
                this.setState({ is_loading: true })
            }
        }
        else {
            this.validateInputFields(this.state.Old_password, 'is_valid_OldPass');
            this.validateInputFields(this.state.New_Password, 'is_valid_NewPass');
            this.validateInputFields(this.state.Confirm_Password, 'is_valid_ConPass');
        }
    }

    HandleChangePassword_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({
                is_loading: false
            })
            this.props.navigation.navigate('Dashboard')
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleChangePassword_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <View style={Common_Style.Common_Header_View}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()} style={Common_Style.Common_BackTouch}>
                        <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../../assets/images/White_BackArrow.png')}
                                style={Common_Style.Common_Header_Icon} />
                        </View>
                    </TouchableOpacity>
                    <Title style={{ fontFamily: 'Segoe UI' }}>Change Password</Title>
                </View>
                <Content>
                    <ScrollView keyboardShouldPersistTaps='always'>
                        <View>
                            <View>
                                <Image
                                    source={require('../../assets/images/Logo196.png')}
                                    style={Common_Style.ChangePwd_img}
                                />
                            </View>
                            <View style={Common_Style.ChangePwd_CardView}>
                                <View style={{ marginTop: '10%' }} >
                                    <Text style={Common_Style.Lable_Txt}>
                                        Old Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={require('../../assets/images/password.png')}
                                                style={{ width: 20, height: 20, marginHorizontal: '2%' }} />
                                            <TextInput
                                                value={this.state.Old_password}
                                                onChangeText={(text) => this.setState({ Old_password: text })}
                                                secureTextEntry={this.state.Old_hidePassword}
                                                style={{ fontFamily: 'Segoe UI', fontSize: Color.common_Small, width: '75%' }}
                                                selectionColor={'#000'}
                                                placeholder={'Old Password'}
                                            />
                                            <View style={{ alignSelf: 'center' }}>
                                                <TouchableOpacity onPress={() => this.Old_PasswordVisibility()}>
                                                    <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={(this.state.Old_hidePassword) ?
                                                            require('../../assets/images/hidePass.png') :
                                                            require('../../assets/images/SHowPass.png')}
                                                            style={Common_Style.eye_Icon} />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                {this.state.is_valid_OldPass == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Enter Old Password
                                    </Text> : null}
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        New Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={require('../../assets/images/password.png')}
                                                style={{ width: 20, height: 20, marginHorizontal: '2%' }} />
                                            <TextInput
                                                value={this.state.New_Password}
                                                onChangeText={(text) => this.validatePassword(text)}
                                                secureTextEntry={this.state.New_hidePassword}
                                                style={{ fontFamily: 'Segoe UI', fontSize: Color.common_Small, width: '75%' }}
                                                selectionColor={'#000'}
                                                placeholder={'New Password'}
                                            />
                                            <View>
                                                <TouchableOpacity onPress={() => this.New_PasswordVisibility()} >
                                                    <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={(this.state.New_hidePassword) ?
                                                            require('../../assets/images/hidePass.png') :
                                                            require('../../assets/images/SHowPass.png')}
                                                            style={Common_Style.eye_Icon} />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                {this.state.is_valid_NewPass == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Enter New Password
                                    </Text> : null}
                                {this.state.pass_validation == false && this.state.is_valid_NewPass == true ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Password must contain atleast 8 characters
                                    </Text> : null}
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Confirm Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Image source={require('../../assets/images/password.png')}
                                                style={{ width: 20, height: 20, marginHorizontal: '2%' }} />
                                            <TextInput
                                                value={this.state.Confirm_Password}
                                                onChangeText={(text) => this.setState({ Confirm_Password: text })}
                                                secureTextEntry={this.state.Confirm_hidePassword}
                                                style={{ fontFamily: 'Segoe UI', fontSize: Color.common_Small, width: '75%' }}
                                                selectionColor={'#000'}
                                                placeholder={'Confirm Password'}
                                            />
                                            <View>
                                                <TouchableOpacity onPress={() => this.Confirm_PasswordVisibility()} >
                                                    <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={(this.state.Confirm_hidePassword) ?
                                                            require('../../assets/images/hidePass.png') :
                                                            require('../../assets/images/SHowPass.png')}
                                                            style={Common_Style.eye_Icon} />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                {this.state.is_valid_ConPass == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Re-enter Password
                                    </Text> : null}

                                <View style={{ marginTop: '10%', marginBottom: '10%' }}>
                                    {this.state.is_loading ? <Loading /> :
                                        <TouchableOpacity style={Common_Style.ForgotPwd_Btn_View} onPress={() => { this.Handle_ChangePass() }} >
                                            <Text style={Common_Style.ForgotPwd_Btn_Txt}>SAVE</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}