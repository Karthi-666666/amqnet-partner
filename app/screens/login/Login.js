import React, { Component } from 'react';
import { BackHandler, Alert, TextInput, View, Image, TouchableOpacity, ScrollView, AsyncStorage } from 'react-native';
import { Container, Content, Text, } from 'native-base';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Login extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            User_id: '',
            login_id: '',
            userpassword: '',
            hidePassword: true,
            is_valid_mobile: true,
            is_valid_password: true,
            is_loading: false,
        };
    }

    PasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    }

    backPressed = () => {
        if (this.props.navigation.isFocused()) {
            Alert.alert(
                "AMQNET",
                "Do you want to exit?",
                [
                    {
                        text: "No",
                        onPress: () => "Cancel Pressed",
                        style: "cancel"
                    },
                    { text: "Yes", onPress: () => BackHandler.exitApp() }
                ],
                { cancelable: false }
            );
            return true
        }
    }

    signup = () => {
        this.props.navigation.navigate('Register')
        this.textmobile.clear()
        this.textPassword.clear()
    }

    forgot_password = () => {
        this.props.navigation.navigate('Forgot_Password')
        this.textmobile.clear()
        this.textPassword.clear()
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    storeUserDetails = async (data) => {
        try {
            await AsyncStorage.setItem('VENDOR_DETAILS', JSON.stringify(data))
        }
        catch (error) {
        }
    }

    bank_acc_status = async (data) => {
        try {
            await AsyncStorage.setItem('Bank_acc_status', JSON.stringify(data))
        }
        catch (error) {
        }
    }

    Remember_Me = async () => {
        try {
            await AsyncStorage.setItem('VENDOR_STAY_LOGGED_IN', 'YES');
        } catch (error) {
        }
    };

    Handle_Login = () => {
        if (this.state.login_id.trim() != "" && this.state.userpassword.trim() != "") {
            this.setState({
                is_valid_mobile: true,
                is_valid_password: true
            })
            Params.Login.user_type = "4",
                Params.Login.loginid = this.state.login_id,
                Params.Login.password = this.state.userpassword
            Api.Post_Login(Params.Login, this.HandleLogin_Response, this.HandleLogin_Error)
            this.setState({ is_loading: true })
        } else {
            this.validateInputFields(this.state.login_id, 'is_valid_mobile');
            this.validateInputFields(this.state.userpassword, 'is_valid_password');
        }
    }

    HandleLogin_Response = async (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.storeUserDetails(response.result)
            this.bank_acc_status(response.bank_acc_status)
            this.Remember_Me()
            this.setState({ is_loading: false })
            let TOKEN = await AsyncStorage.getItem('Vendor_fcmToken');
            Params.InsertToken.user_id = response.result[0].user_id,
                Params.InsertToken.token = TOKEN
            Api.Post_Insert_Token(Params.InsertToken, this.HandleToken_Response, this.HandleToken_Error)
            this.textmobile.clear()
            this.textPassword.clear()
            if (response.bank_acc_status == 0) {
                this.props.navigation.navigate('Bank_Details')
            } else {
                this.props.navigation.navigate('Dashboard')
            }
        }
        else if (response.vendor_status == 0) {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ User_id: response.user_id })
            this.textmobile.clear()
            this.textPassword.clear()
            this.props.navigation.navigate('Upload_Documents', { User_id: this.state.User_id })
            this.setState({ is_loading: false })
        }
        else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleToken_Response = (response) => {
        if (response.status == 'success') {
            this.setState({ is_loading: false })
        } else {
            this.setState({ is_loading: false })
        }
    }

    HandleToken_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    HandleLogin_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <Content >
                    <ScrollView keyboardShouldPersistTaps='always'>
                        <View>
                            <View>
                                <Image
                                    source={require('../../assets/images/Logo196.png')}
                                    style={Common_Style.Login_img}
                                />
                            </View>
                            <View style={Common_Style.Login_CardView}>
                                <Text style={Common_Style.login_Txt}>Login</Text>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Mobile number
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            ref={input => { this.textmobile = input }}
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.setState({ login_id: text })}
                                            selectionColor={'#000'}
                                            placeholder={'Mobile number'}
                                            keyboardType="number-pad"
                                            maxLength={10}
                                        />
                                    </View>
                                </View>
                                {this.state.is_valid_mobile == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Enter Mobile number
                                    </Text> : null}
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <View style={{ flexDirection: 'row' }}>
                                            <TextInput
                                                ref={input => { this.textPassword = input }}
                                                value={this.state.userpassword}
                                                onChangeText={(text) => this.setState({ userpassword: text })}
                                                secureTextEntry={this.state.hidePassword}
                                                style={Common_Style.Txt_Input}
                                                selectionColor={'#000'}
                                                placeholder={'Password'}
                                                enabled={true}
                                            />
                                            <View style={{ alignSelf: 'center' }}>
                                                <TouchableOpacity onPress={() => this.PasswordVisibility()} >
                                                    <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                        <Image source={(this.state.hidePassword) ?
                                                            require('../../assets/images/hidePass.png') :
                                                            require('../../assets/images/SHowPass.png')}
                                                            style={Common_Style.eye_Icon} />
                                                    </View>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                                {this.state.is_valid_password == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Enter Password
                                    </Text> : null}
                                <View style={Common_Style.Re_Forgot_View}>
                                    <TouchableOpacity onPress={() => { this.forgot_password() }}>
                                        <Text style={Common_Style.small_Txt}>Forgot Password ?</Text>
                                    </TouchableOpacity>
                                </View>
                                <View style={{ marginTop: '10%' }}>
                                    {this.state.is_loading ? <Loading /> :
                                        <TouchableOpacity style={Common_Style.Login_Btn_View} onPress={() => { this.Handle_Login() }} >
                                            <Text style={Common_Style.Login_Btn_Txt}>LOGIN</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                                <Text style={Common_Style.OR_Txt}>OR</Text>
                                <View style={Common_Style.SingUp_View}>
                                    <Text style={Common_Style.small_Txt}>Don't have an account?</Text>
                                    <TouchableOpacity onPress={() => { this.signup() }}>
                                        <Text style={Common_Style.SingUp_Txt}>SIGNUP</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}