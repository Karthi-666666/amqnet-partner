import React, { Component } from 'react';
import { Container } from 'native-base';
import { Image, AsyncStorage } from 'react-native';
import Styles from '../../assets/styles/style';

export default class Splash extends Component {

    static navigationOptions = {
        header: null
    }

    UNSAFE_componentWillMount() {
        setTimeout(() => {
            this.Login_AsyncStorage()
        }, 5000);
    }

    async Login_AsyncStorage() {
        let value = await AsyncStorage.getItem('VENDOR_STAY_LOGGED_IN');
        if (value !== null) {
            let Bank_acc_status = await AsyncStorage.getItem('Bank_acc_status');
            if (Bank_acc_status == 0) {
                this.props.navigation.navigate('Bank_Details')
            } else {
                this.props.navigation.navigate('Dashboard')
            }
        } else {
            this.props.navigation.navigate('Login')
        }
    }

    render() {
        return (
            <Container style={Styles.splash_view}>
                <Image
                    source={require('../../assets/images/Logo196.png')}
                    style={Styles.splash_image}
                />
            </Container>
        )
    }
}