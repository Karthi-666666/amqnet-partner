import React, { Component } from 'react';
import { View, ScrollView, AsyncStorage, TextInput, TouchableOpacity, Image, BackHandler, Alert } from 'react-native';
import { Container, Content, Text, Title, Button } from 'native-base';
import Api from '../../http_services/Api';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';
import { DrawerActions } from 'react-navigation-drawer';

export default class Bank_Details extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            Ac_Holder_name: '',
            is_Valid_Ac_Holder_name: true,
            Ac_Number: '',
            is_Valid_Ac_Number: true,
            IFSC_Code: '',
            is_Valid_IFSC_Code: true,
            is_loading: true,
            contact_id: ''
        }
    }

    componentDidMount() {
        BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    }
    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
    }
    backPressed = () => {
        if (this.props.navigation.isFocused()) {
            Alert.alert(
                "AMQNET",
                "Do you want to exit?",
                [
                    {
                        text: "No",
                        onPress: () => "Cancel Pressed",
                        style: "cancel"
                    },
                    { text: "Yes", onPress: () => BackHandler.exitApp() }
                ],
                { cancelable: false }
            );
            return true
        }
    }

    async componentWillMount() {
        let value = await AsyncStorage.getItem('VENDOR_DETAILS');
        this.setState({ user_details: JSON.parse(value) });
        Api.Get_Payout_Contact_ID(this.contactID_Response, this.contactID_Error)
    }

    contactID_Response = (response) => {
        if (response.status == 'success') {
            this.setState({ contact_id: response.contact_id, is_loading: false })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    contactID_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    Handle_Submil = () => {
        if (this.state.Ac_Holder_name != "" && this.state.Ac_Number != "" && this.state.IFSC_Code != "") {
            this.setState({ is_Valid_Ac_Holder_name: true, is_Valid_Ac_Number: true, is_Valid_IFSC_Code: true })
            let param = {
                "account_type": "bank_account",
                "user_id": this.state.user_details[0].user_id,
                "Account_holder_name": this.state.Ac_Holder_name,
                "Account_number": this.state.Ac_Number,
                "IFSC_code": this.state.IFSC_Code,
                "contact_id": this.state.contact_id,
            }
            Api.Post_Bank_Details(param, this.Bank_Details_Response, this.Bank_Details_Error)
            this.setState({ is_Btnloading: true })
        } else {
            this.validateInputFields(this.state.Ac_Holder_name, 'is_Valid_Ac_Holder_name');
            this.validateInputFields(this.state.Ac_Number, 'is_Valid_Ac_Number');
            this.validateInputFields(this.state.IFSC_Code, 'is_Valid_IFSC_Code');
        }
    }

    Bank_Details_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ is_Btnloading: false })
            this.updateBankDetailsStatus(response.bank_acc_status);
            this.props.navigation.navigate('Dashboard')
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Btnloading: false })
        }
    }

    Bank_Details_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_Btnloading: false })
    }

    updateBankDetailsStatus = async (data) => {
        try {
            await AsyncStorage.setItem('Bank_acc_status', JSON.stringify(data));
        } catch (error) {
        }
    };

    DrawerOpen() {
        this.props.navigation.dispatch(DrawerActions.toggleDrawer())
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <View style={Common_Style.Common_Header_View}>
                    <TouchableOpacity
                        onPress={() => { this.DrawerOpen() }}
                        style={Common_Style.Common_BackTouch}>
                        <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../../assets/images/menu.png')}
                                style={Common_Style.Common_Header_Icon} />
                        </View>
                    </TouchableOpacity>
                    <Title style={{ fontFamily: 'Segoe UI' }}>Bank Details</Title>
                </View>
                <Content>
                    <ScrollView keyboardShouldPersistTaps='on-drag' >
                        <View style={{ width: '100%', marginTop: '20%' }}>
                            <View style={{ paddingHorizontal: '5%', backgroundColor: Color.common_White, width: "90%", alignSelf: 'center', borderRadius: 10 }}>
                                <View style={{ marginBottom: '10%' }}>
                                    <Text style={{ paddingVertical: '3%', fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>Account Holder Name</Text>
                                    <View style={{ width: '90%', borderColor: Color.Txt_Gray, borderWidth: 1, borderRadius: 5, height: 40 }}>
                                        <TextInput
                                            value={this.state.Ac_Holder_name}
                                            onChangeText={(text) => this.setState({ Ac_Holder_name: text })}
                                            style={{ fontSize: Color.common_Small, width: '90%', paddingLeft: '3%', fontFamily: 'Segoe UI' }}
                                            selectionColor={'#000'}
                                        />
                                    </View>
                                    {this.state.is_Valid_Ac_Holder_name == false ?
                                        <Text style={Common_Style.Document_Error_Txt}>
                                            Please Enter Account Holder Name
                                    </Text> : null}
                                    <Text style={{ paddingVertical: '3%', fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>Account Number</Text>
                                    <View style={{ width: '90%', borderColor: Color.Txt_Gray, borderWidth: 1, borderRadius: 5, height: 40 }}>
                                        <TextInput
                                            value={this.state.Ac_Number}
                                            onChangeText={(text) => this.setState({ Ac_Number: text })}
                                            style={{ fontSize: Color.common_Small, width: '90%', paddingLeft: '3%', fontFamily: 'Segoe UI' }}
                                            selectionColor={'#000'}
                                            keyboardType="number-pad"
                                        />
                                    </View>
                                    {this.state.is_Valid_Ac_Number == false ?
                                        <Text style={Common_Style.Document_Error_Txt}>
                                            Please Enter Account Number
                                    </Text> : null}
                                    <Text style={{ paddingVertical: '3%', fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>IFSC Code</Text>
                                    <View style={{ width: '90%', borderColor: Color.Txt_Gray, borderWidth: 1, borderRadius: 5, height: 40 }}>
                                        <TextInput
                                            value={this.state.IFSC_Code}
                                            onChangeText={(text) => this.setState({ IFSC_Code: text })}
                                            style={{ fontSize: Color.common_Small, width: '90%', paddingLeft: '3%', fontFamily: 'Segoe UI' }}
                                            selectionColor={'#000'}
                                        />
                                    </View>
                                    {this.state.is_Valid_IFSC_Code == false ?
                                        <Text style={Common_Style.Document_Error_Txt}>
                                            Please Enter IFSC Code
                                    </Text> : null}
                                </View>
                            </View>
                            <View style={{ marginTop: '15%', marginBottom: '5%' }}>
                                {this.state.is_Btnloading ? <Loading /> :
                                    <Button
                                        style={{ borderWidth: 1, borderColor: Color.common_Green, borderRadius: 5, width: '90%', height: 45, alignSelf: 'center', backgroundColor: Color.common_Green, justifyContent: 'center' }}
                                        onPress={this.Handle_Submil}
                                    >
                                        <Text style={{ textAlign: 'center', color: Color.common_White, fontFamily: 'Segoe UI' }}>Submit</Text>
                                    </Button>
                                }
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container >
        )
    }
}