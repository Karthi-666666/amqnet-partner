import React, { Component } from 'react';
import { TextInput, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Container, Content, Text } from 'native-base';
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Forgot_Password extends Component {

    static navigationOptions = {
        header: null
    }
    constructor(props) {
        super(props);
        this.state = {
            User_Mobile: '',
            is_valid_mobile: true,
            is_loading: false,
        };
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    Handle_ForgotPwd = () => {
        if (this.state.User_Mobile.trim() != "") {
            this.setState({ is_valid_mobile: true, })
            Params.ForgotPassword.email_or_mobile = this.state.User_Mobile
            Api.Post_ForgotPassword(Params.ForgotPassword, this.HandleForgotPassword_Response, this.HandleForgotPassword_Error)
            this.setState({ is_loading: true })
        } else {
            this.validateInputFields(this.state.User_Mobile, 'is_valid_mobile');
        }
    }

    HandleForgotPassword_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ is_loading: false })
            this.props.navigation.navigate('OTP', { mobile_No: response.result[0].mobile, screen: 'FORGOTPASSWORD' })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleForgotPassword_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <Content >
                    <ScrollView>
                        <View>
                            <View>
                                <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                                    <View style={{ height: 60, width: 60, justifyContent: 'center', alignItems: 'center' }}>
                                        <Image source={require('../../assets/images/Black_BackArrow.png')}
                                            style={Common_Style.ForgotPwd_BackIcon}
                                        />
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View>
                                <Image
                                    source={require('../../assets/images/Logo196.png')}
                                    style={Common_Style.ForgotPwd_img}
                                />
                            </View>
                            <View style={Common_Style.ForgotPwd_CardView}>
                                <Text style={Common_Style.ForgotPwd_Txt}>Forgot Password</Text>
                                <View style={{ marginTop: '5%' }}>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Enter Mobile number
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.setState({ User_Mobile: text })}
                                            selectionColor={'#000'}
                                            placeholder={'Mobile number'}
                                            keyboardType="number-pad"
                                            maxLength={10}
                                        />
                                    </View>
                                </View>
                                {this.state.is_valid_mobile == false ?
                                    <Text style={Common_Style.In_Error_Txt}>
                                        Please Enter Mobile number
                                    </Text> : null}
                                <View style={{ marginTop: '25%', marginBottom: '20%' }}>
                                    {this.state.is_loading ? <Loading /> :
                                        <TouchableOpacity style={Common_Style.ForgotPwd_Btn_View} onPress={() => { this.Handle_ForgotPwd() }} >
                                            <Text style={Common_Style.ForgotPwd_Btn_Txt}>SUBMIT</Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}