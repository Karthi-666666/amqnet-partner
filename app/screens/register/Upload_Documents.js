import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Container, Content, Text } from 'native-base';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import DocumentPicker from 'react-native-document-picker';
import Color from '../../assets/colors/colors';
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Upload_Document extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            is_Btnloading: false,
            User_id: this.props.navigation.getParam('User_id'),
            Vendor_Document: '',
            doc_details: [],
            Down_Arrow: true,
        };
    }

    async  Attachments() {
        try {
            const results = await DocumentPicker.pickMultiple({
                type: [DocumentPicker.types.images, DocumentPicker.types.pdf],
            });
            for (const res of results) {
                if (this.state.doc_details.length <= 4) {
                    this.state.doc_details.push(res)
                    this.setState({ doc_details: this.state.doc_details })
                }
                else {
                    ToastMsg('danger', 'Select maximum five document', Color.toast_danger)
                }
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
            } else {
                throw err;
            }
        }
    }

    remove_Document = (index) => {
        var array = [...this.state.doc_details];
        array.splice(index, 1);
        this.setState({ doc_details: array });
    }

    Upload_all_documents = () => {
        if (this.state.doc_details != '') {
            Upload_Data = new FormData();
            this.state.doc_details.map((data, index) => {
                Upload_Data.append('uploaded_file[]', {
                    uri: data.uri,
                    type: data.type,
                    name: data.name,
                });
            })
            Api.Post_Vendor_Signupimageupload(Upload_Data, this.HandleUploadDocuments_Response, this.HandleUploadDocuments_Error)
            this.setState({ is_Btnloading: true })
        } else (
            ToastMsg('danger', 'Select minimum one document', Color.toast_danger),
            this.setState({ is_Btnloading: false })
        )
    }

    HandleUploadDocuments_Response = (response) => {
        if (response.status == 'success') {
            this.setState({
                Vendor_Document: response.result[0],
            })
            this.Vendor_imageinsert()
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Btnloading: false })
        }
    }

    HandleUploadDocuments_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_Btnloading: false })
    }

    Vendor_imageinsert() {
        Params.Vendor_DocumentsInsert.user_id = this.state.User_id,
            Params.Vendor_DocumentsInsert.image0 = this.state.Vendor_Document.image0,
            Params.Vendor_DocumentsInsert.image1 = this.state.Vendor_Document.image1,
            Params.Vendor_DocumentsInsert.image2 = this.state.Vendor_Document.image2,
            Params.Vendor_DocumentsInsert.image3 = this.state.Vendor_Document.image3,
            Params.Vendor_DocumentsInsert.image4 = this.state.Vendor_Document.image4,
            Api.Post_Vendor_Signupimageinsert(Params.Vendor_DocumentsInsert, this.HandVendor_imageinsert_Response, this.HandleVendor_imageinsert_Error)
        this.setState({ is_Btnloading: true })
    }

    HandVendor_imageinsert_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ is_Btnloading: false })
            this.props.navigation.navigate('Login')
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_Btnloading: false })
        }
    }

    HandleVendor_imageinsert_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_Btnloading: false })
    }

    Up_Down_Arrow = () => {
        this.setState({ Down_Arrow: !this.state.Down_Arrow })
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <Content>
                    <ScrollView keyboardShouldPersistTaps='on-drag' >
                        <View style={{ marginTop: '15%', width: '100%' }}>
                            <View style={{ width: '90%', alignSelf: 'center', backgroundColor: Color.common_White, borderRadius: 10 }}>
                                <View style={{ flexDirection: 'row', paddingVertical: '3%' }}>
                                    <View style={{ width: '90%' }}>
                                        <TouchableOpacity onPress={() => { this.Attachments() }} >
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Image source={require('../../assets/images/Upload.png')} style={{ height: 20, width: 20, marginHorizontal: '5%' }} />
                                                <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI Bold' }}>Upload Document</Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                    <TouchableOpacity onPress={() => { this.Up_Down_Arrow() }} >
                                        <Image source={(this.state.Down_Arrow) ?
                                            require('../../assets/images/Down.png') :
                                            require('../../assets/images/Up.png')}
                                            style={{ height: 20, width: 20 }}
                                        />
                                    </TouchableOpacity>
                                </View>
                                {(this.state.Down_Arrow == true) ? null :
                                    <View style={{ paddingLeft: '3%' }}>
                                        <View style={{ flexDirection: 'row', marginTop: '3%' }}>
                                            <View style={{ paddingHorizontal: '3%' }}>
                                                <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>1 </Text>
                                            </View>
                                            <View>
                                                <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>To make sure that you are autenticated user,</Text>
                                                <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>Upload your certified registered service pdf/excel/png document.</Text>
                                            </View>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: '3%', marginBottom: '3%' }}>
                                            <View style={{ paddingHorizontal: '3%' }}>
                                                <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}>2</Text>
                                            </View>
                                            <View>
                                                <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>To Know your identity,</Text>
                                                <Text style={{ fontSize: Color.common_Small, fontFamily: 'Segoe UI', width: '70%' }}>Upload Passport photo/Adhar card photo/Voter Id/Pan card photo.</Text>
                                            </View>
                                        </View>
                                    </View>}
                            </View>
                            <View style={{ marginTop: '5%' }}>
                                {this.state.doc_details.map((data, index) => {
                                    return (
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                            <Text style={{ fontSize: Color.common_Medium, fontFamily: 'Segoe UI Bold', color: Color.common_White, marginLeft: '5%', paddingVertical: '2%' }}>
                                                {data.name}
                                            </Text>
                                            <TouchableOpacity onPress={() => { this.remove_Document(index) }}>
                                                <View style={{ height: 40, width: 40, alignItems: 'center', justifyContent: 'center' }}>
                                                    <Image source={require('../../assets/images/cancel.png')} style={{ backgroundColor: Color.common_Black, borderRadius: 10, height: 15, width: 15 }} />
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    )
                                })}
                            </View>
                            <View style={{ marginTop: '5%', marginBottom: '5%' }}>
                                {this.state.is_Btnloading ? <Loading /> :
                                    <TouchableOpacity
                                        style={{ borderWidth: 1, borderColor: Color.common_Green, borderRadius: 5, width: '90%', height: 45, alignSelf: 'center', backgroundColor: Color.common_Green, justifyContent: 'center' }}
                                        onPress={() => { this.Upload_all_documents() }} >
                                        <Text style={{ textAlign: 'center', color: Color.common_White, fontFamily: 'Segoe UI' }}>Submit</Text>
                                    </TouchableOpacity>
                                }
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container >
        )
    }
}