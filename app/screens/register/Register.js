import React, { Component } from 'react';
import { TextInput, View, Image, TouchableOpacity, ScrollView, CheckBox, } from 'react-native';
import { Container, Content, Text, } from 'native-base';
import { Dropdown } from 'react-native-material-dropdown';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import Common_Style from '../../assets/styles/style';
import Color from '../../assets/colors/colors';
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Register extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            Vendors_Type_list: [],
            name: '',
            mobile_no: '',
            pincode: '',
            address: '',
            password: '',
            confirm_pass: '',
            Upload_Document: '',
            VendorType_name: '',
            VendorType_id: '',
            i_agree: false,
            is_loading: false,
            is_valid_name: true,
            is_valid_Vendor_id: true,
            is_valid_pincode: true,
            is_valid_address: true,
            is_valid_password: true,
            is_valid_MobileNo: true,
            is_valid_confirm_pass: true,
            is_valid_Upload_Document: true,
            pass_validation: true,
            MobileNo_validation: true,
            DocumentName: '',
            DocumentURL: '',
            is_Uploaded: false,
            isModalVisible: false,
            Vendor_Document: '',
        };
    }

    componentWillMount() {
        Api.get_Vendors_Type_list(this.handleVendors_Type_list_Response, this.handleVendors_Type_list_Error)
    }

    handleVendors_Type_list_Response = (response) => {
        if (response.status == 'success') {
            this.setState({ Vendors_Type_list: response.response })
        } else {
            ToastMsg('danger', response.result, Color.toast_danger)
        }
    }

    handleVendors_Type_list_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }

    validatePassword = (text) => {
        this.setState({
            pass_validation: text.length <= 7 ? false : true,
            password: text,
            is_valid_password: true
        })
    }

    validateMobileNo = (text) => {
        this.setState({
            MobileNo_validation: text.length <= 9 ? false : true,
            mobile_no: text,
            is_valid_MobileNo: true
        })
    }

    validateInputFields = (fields, value) => {
        this.setState({
            [value]: fields == '' ? false : true,
        })
    }

    Handle_SignUp = () => {
        if (this.state.i_agree == true) {
            if (this.state.name.trim() != "" && this.state.mobile_no.trim() != "" && this.state.VendorType_id != "" && this.state.pincode.trim() != "" && this.state.address.trim() != "" && this.state.password.trim() != "" && this.state.confirm_pass.trim() != "" && this.state.pass_validation == true && this.state.MobileNo_validation == true) {
                this.setState({
                    is_valid_name: true,
                    is_valid_pincode: true,
                    is_valid_Vendor_id: true,
                    is_valid_address: true,
                    is_valid_password: true,
                    is_valid_MobileNo: true,
                    is_valid_confirm_pass: true,
                    is_valid_Upload_Document: true
                })
                if (this.state.password != this.state.confirm_pass) {
                    ToastMsg('danger', 'Password mismatched!', Color.toast_danger)
                } else {
                    Params.SignUp.user_type = "4",
                        Params.SignUp.user_name = this.state.name,
                        Params.SignUp.mobile = this.state.mobile_no,
                        Params.SignUp.email = null,
                        Params.SignUp.apartment_id = null,
                        Params.SignUp.pincode = this.state.pincode,
                        Params.SignUp.password = this.state.password,
                        Params.SignUp.address = this.state.address,
                        Params.SignUp.vendors_type_id = this.state.VendorType_id,
                        Params.SignUp.flat_no = null,
                        Params.SignUp.vendor_document = null,
                        Api.Post_SignUp(Params.SignUp, this.HandleRegister_Response, this.HandleRegister_Error)
                    this.setState({ is_loading: true })
                }
            } else {
                this.validateInputFields(this.state.name, 'is_valid_name');
                this.validateInputFields(this.state.pincode, 'is_valid_pincode');
                this.validateInputFields(this.state.VendorType_id, 'is_valid_Vendor_id');
                this.validateInputFields(this.state.address, 'is_valid_address');
                this.validateInputFields(this.state.password, 'is_valid_password');
                this.validateInputFields(this.state.mobile_no, 'is_valid_MobileNo');
                this.validateInputFields(this.state.confirm_pass, 'is_valid_confirm_pass');
                this.validateInputFields(this.state.Upload_Document, 'is_valid_Upload_Document');
            }
        } else {
            alert('Please agree terms and conditions')
        }
    }

    HandleRegister_Response = (response) => {
        if (response.status == 'success') {
            ToastMsg('success', response.message, Color.toast_success)
            this.setState({ user_details: response.result })
            this.setState({ is_loading: false })
            this.props.navigation.navigate('OTP', { mobile_No: this.state.mobile_no, screen: 'REGISTER' })
        } else {
            ToastMsg('danger', response.message, Color.toast_danger)
            this.setState({ is_loading: false })
        }
    }

    HandleRegister_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ is_loading: false })
    }
    Vendor_TypeList = (value, index) => {
        this.setState({
            VendorType_name: value,
            VendorType_id: this.state.Vendors_Type_list[index].id,
        })
    }

    Login = () => {
        this.props.navigation.navigate('Login')
    }

    Terms_Condition = () => {
        this.props.navigation.navigate('Terms_Conditions')
    }

    Privacy_Policy = () => {
        this.props.navigation.navigate('Privacy_Policy')
    }

    render() {
        return (
            <Container style={{ backgroundColor: Color.common_gray }}>
                <Content >
                    <ScrollView keyboardShouldPersistTaps='on-drag' >
                        <View>
                            <View>
                                <Image
                                    source={require('../../assets/images/Logo196.png')}
                                    style={Common_Style.Register_img}
                                />
                            </View>
                            <View style={Common_Style.Register_CardView}>
                                <Text style={Common_Style.Register_Txt}>SignUp</Text>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Enter Name
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            onChangeText={(text) => this.setState({ name: text })}
                                            selectionColor={'#000'}
                                            placeholder={'Name'}
                                            style={Common_Style.Txt_Input}
                                        />
                                    </View>
                                    {this.state.is_valid_name == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Name
                                    </Text> : null}
                                </View>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Mobile number
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            value={this.state.mobile_no}
                                            onChangeText={(text) => this.validateMobileNo(text)}
                                            selectionColor={'#000'}
                                            keyboardType="number-pad"
                                            maxLength={10}
                                            placeholder={'Mobile number'}
                                            style={Common_Style.Txt_Input}
                                        />
                                    </View>
                                    {this.state.is_valid_MobileNo == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Mobile Number
                                     </Text> : null}
                                    {this.state.MobileNo_validation == false && this.state.is_valid_MobileNo == true ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Mobile number must contain 10 numbers
                                    </Text> : null}
                                </View>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Select Service Type
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <View style={{ flexDirection: 'row' }}>
                                            <Dropdown
                                                itemTextStyle={{ textAlign: 'center', }}
                                                style={{ fontFamily: 'ProximaNova-Regular', fontSize: Color.common_Small, fontFamily: 'Segoe UI' }}
                                                containerStyle={Common_Style.Dropdown_Container}
                                                underlineColorAndroid='transparent'
                                                placeholder='-- Select Service Type --'
                                                dropdownPosition={0}
                                                data={this.state.Vendors_Type_list}
                                                onChangeText={(value, index) => { this.Vendor_TypeList(value, index) }}
                                            />
                                        </View>
                                    </View>
                                    {this.state.is_valid_Vendor_id == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Select Vendor Type
                                    </Text> : null}
                                </View>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Pin Code
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.setState({ pincode: text })}
                                            selectionColor={'#000'}
                                            keyboardType="number-pad"
                                            maxLength={6}
                                            placeholder={' Pin Code'} />
                                    </View>
                                    {this.state.is_valid_pincode == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Pincode
                                    </Text> : null}
                                </View>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Address
                                    </Text>
                                    <View style={{ borderWidth: 1, borderColor: Color.common_White, borderRadius: 5, width: '90%', height: 110, alignSelf: 'center', backgroundColor: Color.common_White }} >
                                        <TextInput
                                            style={Common_Style.Address_TxtIn}
                                            onChangeText={(text) => this.setState({ address: text })}
                                            selectionColor={'#000'}
                                            placeholder={'Address'}
                                            numberOfLines={2}
                                            multiline={true}
                                        />
                                    </View>
                                    {this.state.is_valid_address == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Address
                                        </Text> : null}
                                </View>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            value={this.state.Password}
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.validatePassword(text)}
                                            selectionColor={'#000'}
                                            secureTextEntry={true}
                                            placeholder={'Password'} />
                                    </View>
                                    {this.state.is_valid_password == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Password
                                    </Text> : null}
                                    {this.state.pass_validation == false && this.state.is_valid_password == true ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Password must contain atleast 8 characters
                                    </Text> : null}
                                </View>
                                <View>
                                    <Text style={Common_Style.Lable_Txt}>
                                        Confirm Password
                                    </Text>
                                    <View style={Common_Style.In_View} >
                                        <TextInput
                                            style={Common_Style.Txt_Input}
                                            onChangeText={(text) => this.setState({ confirm_pass: text })}
                                            selectionColor={'#000'}
                                            secureTextEntry={true}
                                            placeholder={'Confirm Password'}
                                            place />
                                    </View>
                                    {this.state.is_valid_confirm_pass == false ?
                                        <Text style={Common_Style.In_Error_Txt}>
                                            Please Enter Confirm Password
                                    </Text> : null}
                                </View>
                                <View style={Common_Style.Iagree_TxtView} >
                                    <CheckBox
                                        value={this.state.i_agree}
                                        onValueChange={() => this.setState({ i_agree: !this.state.i_agree })}
                                    />
                                    <Text style={Common_Style.small_Txt}>I agree </Text>
                                    <TouchableOpacity onPress={() => this.Privacy_Policy()}>
                                        <Text style={[Common_Style.small_Txt, { textDecorationLine: 'underline' }]}>Privacy policy</Text>
                                    </TouchableOpacity>
                                    <Text style={Common_Style.small_Txt}> & </Text>
                                    <TouchableOpacity onPress={() => this.Terms_Condition()}>
                                        <Text style={[Common_Style.small_Txt, { textDecorationLine: 'underline' }]}>Terms Conditions</Text>
                                    </TouchableOpacity>
                                </View>
                                {this.state.is_loading ? <Loading /> :
                                    <View style={{ marginTop: '5%', marginBottom: '3%' }}>
                                        <TouchableOpacity style={Common_Style.Login_Btn_View} onPress={() => { this.Handle_SignUp() }} >
                                            <Text style={Common_Style.Login_Btn_Txt}>NEXT</Text>
                                        </TouchableOpacity>
                                    </View>
                                }
                                <View style={{ marginBottom: '5%' }}>
                                    <View style={Common_Style.LoginTxt_View}>
                                        <Text style={Common_Style.small_Txt}>Do you have an account?</Text>
                                        <TouchableOpacity onPress={() => { this.Login() }}>
                                            <Text style={Common_Style.LoginTxt}>LOGIN</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
            </Container>
        )
    }
}