import React, { Component } from 'react';
import { View, Image, TouchableOpacity, ScrollView } from 'react-native';
import { Container, Title, Text, Content } from 'native-base';
import Color from '../../assets/colors/colors'
import Common_Style from '../../assets/styles/style';
import Api from '../../http_services/Api';
let Params = require('../../assets/json/Params')
import { ToastMsg } from '../../assets/helper/Helper';
import Loading from '../../assets/loading/Loading';

export default class Terms_and_Condition extends Component {

    static navigationOptions = {
        header: null
    }

    constructor() {
        super();
        this.state = {
            isLoading: false,
            Description: ''
        }
    }
    componentWillMount() {
        Params.Terms_and_Condition.user_type = "4",
            Api.Post_Terms_and_Condition(Params.Terms_and_Condition, this.Terms_and_Condition_Response, this.Terms_and_Condition_Error)
        this.setState({ isLoading: true })
    }

    Terms_and_Condition_Response = (response) => {
        if (response.status == 'success') {
            this.setState({
                Description: response.response[0].description,
                isLoading: false
            })
        } else {
            this.setState({ isLoading: false })
        }
    }

    Terms_and_Condition_Error = (error) => {
        ToastMsg('danger', error, Color.toast_danger)
        this.setState({ isLoading: false })
    }

    render() {
        return (
            <Container>
                <View style={Common_Style.Common_Header_View}>
                    <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                        <View style={{ width: 40, height: 40, alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={require('../../assets/images/White_BackArrow.png')}
                                style={Common_Style.Common_Header_Icon} />
                        </View>
                    </TouchableOpacity>
                    <Title style={{ fontFamily: 'Segoe UI' }}>Terms & Condition</Title>
                </View>
                <Content>
                    <ScrollView>
                        {this.state.isLoading ? <Loading /> :
                            <View style={{ width: "90%", alignSelf: 'center', marginTop: '10%', marginBottom: '10%' }}>
                                <Text style={{ fontSize: Color.common_Medium, fontFamily: 'Segoe UI' }}>{this.state.Description}</Text>
                            </View>
                        }
                    </ScrollView>
                </Content>
            </Container>
        );
    }
}