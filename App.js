import React, { Component } from 'react';
import { Root } from "native-base";
import Drawernavigator from './app/navigation/Drawernavigator';
import { StatusBar, AsyncStorage } from 'react-native';
import firebase from 'react-native-firebase';
console.disableYellowBox = true;

class App extends Component {
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('Vendor_fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        await AsyncStorage.setItem('Vendor_fcmToken', fcmToken);
      }
    }
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      this.getToken();
    } catch (error) {
    }
  }

  async createNotificationListeners() {
    firebase.notifications().onNotification(notification => {
      notification.android.setChannelId('insider').setSound('default')
      firebase.notifications().displayNotification(notification)
    });
  }

  componentDidMount() {
    const channel = new firebase.notifications.Android.Channel('insider', 'insider channel', firebase.notifications.Android.Importance.Max)
    firebase.notifications().android.createChannel(channel);
    this.checkPermission();
    this.createNotificationListeners();
  }
  render() {
    return (
      <Root>
        <StatusBar backgroundColor="#818d66" barStyle="light-content" />
        <Drawernavigator />
      </Root>
    )
  }
}

export default App;
